package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.auriga.automation.common.*;
import com.mycol.automation.factorydata.*;

public class AdminAddHospitalGenral {

	protected WebDriver driver;
	private By hospitallink = By.xpath(".//*[@id='topnav']/div/div[3]/ul/li[1]/a");
	private By addbutton = By.xpath(".//*[@id='content']/a");
	private By pageheading = By.xpath(".//*[@id='content']/h1");

	// Genral object
	private By name = By.id("Hospital_name");
	private By address1 = By.id("Hospital_address1");
	private By address2 = By.id("Hospital_address2");
	private By state = By.id("Hospital_state_id");
	private By city = By.id("Hospital_city_id");
	private By locality = By.id("Hospital_locality_id");
	private By pincode = By.id("Hospital_pincode");
	private By brand = By.id("Hospital_brand_id");
	private By logo = By.name("logo_image");
	private By email = By.id("Hospital_email");
	private By licence = By.id("Hospital_license_number");
	private By established = By.id("Hospital_established_year");
	private By refurbishment = By.id("Hospital_refurbishment_last_year");
	private By telephone = By.id("Hospital_0_telephone_number");
	private By telephoneadd = By
			.xpath(".//*[@id='telephone_number_row_0']/div[2]/input");
	private By fax = By.id("Hospital_0_fax_number");
	private By faxadd = By.xpath(".//*[@id='fax_number_row_0']/div[2]/input");
	private By emergency = By.id("Hospital_0_emergency_contact_number");
	private By emergencyadd = By
			.xpath(".//*[@id='emergency_contact_row_0']/div[2]/input");
	private By ambulance = By.id("Hospital_0_ambulance_helpline_number");
	private By ambulanceadd = By
			.xpath(".//*[@id='ambulance_helpline_number_row_0']/div[2]/input");
	private By accreditation = By.id("Hospital_0_hospital_accreditation");
	private By accreditationadd = By
			.id(".//*[@id='hospital_accreditation_row_0']/div[2]/input");
	private By hospitalabout = By.id("Hospital_about");
	private By genraladd = By
			.xpath(".//*[@id='hospital-form-1']/div[31]/input");
	private By msgsuccess = By.xpath(".//*[@id='msg-hospital-form-1']");
	private By errstate = By.xpath(".//*[@id='hospital-form-1']/div[4]/div[1]");
	private By errname = By.xpath(".//*[@id='hospital-form-1']/div[1]/div");
	private By erraddr1 = By.xpath(".//*[@id='hospital-form-1']/div[2]/div");
	private By errpincode = By.xpath(".//*[@id='hospital-form-1']/div[7]/div");

	public void openManageHospital()

	{
		//driver.findElement(hospitallink).click();
	}

	public void openAddGenralHospitalPage() {
		driver.findElement(addbutton).click();
	}
	
	public void openEditHospital()
	{
		driver.findElement(By.xpath(".//*[@id='hospital-grid']/table/tbody/tr[1]/td[7]/a[1]")).click();
		FrameWork.AcceptAlert(driver);
	}
	public Boolean doAddGenralHospital() {
		clearAll();
		driver.findElement(name).sendKeys(Hospital.genral.nameText);
		driver.findElement(address1).sendKeys(Hospital.genral.address1Text);
		driver.findElement(address2).sendKeys(Hospital.genral.address2Text);
		new Select(driver.findElement(state))
				.selectByVisibleText(Hospital.genral.stateText);
		new Select(driver.findElement(city))
				.selectByVisibleText(Hospital.genral.cityText);
		new Select(driver.findElement(locality))
				.selectByVisibleText(Hospital.genral.localityText);
		driver.findElement(pincode).sendKeys(Hospital.genral.pincodeText);
		/*new Select(driver.findElement(brand))
				.selectByVisibleText(Hospital.genral.brandText);*/
		driver.findElement(logo).sendKeys(Hospital.genral.logoText);
		driver.findElement(email).sendKeys(Hospital.genral.emailText);
		driver.findElement(licence).sendKeys(Hospital.genral.licenceText);
		driver.findElement(established).sendKeys(
				Hospital.genral.establishedText);
		driver.findElement(refurbishment).sendKeys(
				Hospital.genral.refurbishmentText);
		driver.findElement(telephone).sendKeys(Hospital.genral.telephoneText);
		driver.findElement(fax).sendKeys(Hospital.genral.faxText);
		driver.findElement(emergency).sendKeys(Hospital.genral.emergencyText);
		driver.findElement(ambulance).sendKeys(Hospital.genral.ambulanceText);
		driver.findElement(accreditation).sendKeys(
				Hospital.genral.accreditationText);
		// driver.findElement(hospitalabout).sendKeys(Hospital.genral.hospitalaboutText);
		FrameWork.Click(driver, genraladd);
		// driver.findElement(genraladd).click();
		Log.info("Adding hospital..");
		System.out.println("This is success message "
				+ driver.findElement(msgsuccess));
		if (FrameWork.verifySuccessMessage(driver, msgsuccess,
				Hospital.genral.msgsuccessText)) {
			Log.info("Hospital Added Successfully ");
			System.out.println("This is success message "
					+ driver.findElement(msgsuccess));
			return true;

		} else {
			Log.info("Hospital Not Added Successfully ");
			return false;

		}

	}

	public Boolean blankMandatoryField() {
		driver.findElement(name).clear();
		driver.findElement(address1).clear();
		driver.findElement(address2).sendKeys(Hospital.genral.address2Text);
		
		driver.findElement(pincode).clear();
		
		driver.findElement(logo).sendKeys(Hospital.genral.logoText);
		driver.findElement(email).sendKeys(Hospital.genral.emailText);
		driver.findElement(licence).sendKeys(Hospital.genral.licenceText);
		driver.findElement(established).sendKeys(
				Hospital.genral.establishedText);
		driver.findElement(refurbishment).sendKeys(
				Hospital.genral.refurbishmentText);
		driver.findElement(telephone).sendKeys(Hospital.genral.telephoneText);
		driver.findElement(fax).sendKeys(Hospital.genral.faxText);
		driver.findElement(emergency).sendKeys(Hospital.genral.emergencyText);
		driver.findElement(ambulance).sendKeys(Hospital.genral.ambulanceText);
		driver.findElement(accreditation).sendKeys(
				Hospital.genral.accreditationText);
		// driver.findElement(hospitalabout).sendKeys(Hospital.genral.hospitalaboutText);
		FrameWork.Click(driver, genraladd);
		// driver.findElement(genraladd).click();
		Log.info("blankMandatoryField fucntion");
		System.out.println("This is success message "
				+ driver.findElement(msgsuccess));
		if ((FrameWork.verifySuccessMessage(driver, errname,
				Hospital.genral.errnameText))
				&& (FrameWork.verifySuccessMessage(driver, erraddr1,
						Hospital.genral.erraddr1Text))
				&& (FrameWork.verifySuccessMessage(driver, errpincode,
						Hospital.genral.errpincodeText))) {

			Log.info("Displayed All Mandatory error message ");

			return true;

		} else {
			Log.error(" Not Displayed All Mandatory error message  at add genral hospital page ");
			return false;

		}
	}

	public void verifyAddedGenralHospital() {

		if (FrameWork.verifInputFieldData(driver, name,
				Hospital.genral.nameText))
			Log.info("Name passed ");
		if (FrameWork.verifInputFieldData(driver, address1,
				Hospital.genral.address1Text))
			Log.info("Address 1 is passed ");
		if (FrameWork.verifInputFieldData(driver, address2,
				Hospital.genral.address2Text))
			Log.info("Address 2 is passed");
		if (FrameWork.verifInputFieldData(driver, pincode,
				Hospital.genral.pincodeText))
			Log.info("pincode is passed");
		if (FrameWork.verifyDropDownData(driver, state,
				Hospital.genral.stateText))
			Log.info("state is Passed ");
		if (FrameWork
				.verifyDropDownData(driver, city, Hospital.genral.cityText))
			Log.info("city is Passed ");
		if (FrameWork.verifyDropDownData(driver, locality,
				Hospital.genral.localityText))
			Log.info("locality is Passed ");
		if (FrameWork.verifInputFieldData(driver, licence,
				Hospital.genral.licenceText))
			Log.info("licence is passed ");
		if (FrameWork.verifInputFieldData(driver, established,
				Hospital.genral.establishedText))
			Log.info("Established is passed ");
		if (FrameWork.verifInputFieldData(driver, refurbishment,
				Hospital.genral.refurbishmentText))
			Log.info("Refubishment is passed ");
		if (FrameWork.verifInputFieldData(driver, telephone,
				Hospital.genral.telephoneText))
			Log.info("Telephone is passed");
		if (FrameWork.verifInputFieldData(driver, fax, Hospital.genral.faxText))
			Log.info("Fax is passed ");
		if (FrameWork.verifInputFieldData(driver, emergency,
				Hospital.genral.emergencyText))
			Log.info("Emergency is passed ");
		if (FrameWork.verifInputFieldData(driver, ambulance,
				Hospital.genral.ambulanceText))
			Log.info("Ambulance is passed ");
		if (FrameWork.verifInputFieldData(driver, accreditation,
				Hospital.genral.accreditationText))
			Log.info("accreditation is passed ");

	}

	public Boolean doVerifyManageHospital() {
		if (driver.getTitle().matches(Hospital.genral.title))
			return true;
		else
			return false;
	}

	public AdminAddHospitalGenral(WebDriver driver) {
		this.driver = driver;
	}

	public void clearAll(){
		FrameWork.GetElement(driver, name).clear();
		driver.findElement(address1).clear();
		driver.findElement(address2).clear();
		driver.findElement(pincode).clear();
		driver.findElement(email).clear();
		driver.findElement(licence).clear();
		driver.findElement(established).clear();
		driver.findElement(refurbishment).clear();
		driver.findElement(telephone).clear();
		driver.findElement(fax).clear();
		driver.findElement(emergency).clear();
		driver.findElement(ambulance).clear();
		driver.findElement(accreditation).clear();
		// driver.findElement(hospitalabout).sendKeys(Hospital.genral.hospitalaboutText);
	
		// driver.findElement(genraladd).click();
	}

}
