package com.mycol.automation.pagefactory;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.*;
import com.mycol.automation.factorydata.*;

public class AddCluster {

	protected WebDriver driver;
	private String updaturl;

	private By addclusterbutton = By.xpath(".//*[@id='content']/a");
	private By clusetername = By.id("Cluster_cluster_name");
	private By clusterdes = By.id("Cluster_cluster_description");
	private By hospitalname = By.id("Cluster_hospital_id");
	private By hospitalautosuggession = By.partialLinkText("Kailash Hospitals");
	private By search = By.id("search");
	private By submit = By.name("yt0");
	private By addclusterpageheading = By.xpath(".//*[@id='content']/h1");
	private By allblankfielderrormessage = By
			.xpath(".//*[@id='cluster-form']/div[2]");
	private By addedhospitalname = By
			.xpath(".//*[@id='Cluster_hospital_id_option_250']");
	private By hospitalautosuggessionlist = By.partialLinkText("Kailash");

	public AddCluster(WebDriver driver) {
		this.driver = driver;
		// super(driver);
	}

	public Boolean doVerifyClusterManagePage() {
		Log.startTestCase("Manage and Add Cluster page test starting....");
		if (driver.getTitle().matches(Cluster.ClusterData.title)) {

			Log.info("Verified Cluster page title");
			return true;
		} else {
			Log.error("Error on during verifying cluster page title");
			return false;
		}
	}

	public void doOpenManageClusterPage() {
		driver.get(Cluster.ClusterData.clustermanage);
		Log.info("opening manage cluster ");

	}

	public void doOpenAddClusterPage() {
		driver.findElement(addclusterbutton).click();
		Log.info("opening Add cluster ");
	}

	public Boolean doVerifyAddClusterPage() {
		if (driver.getTitle().matches(Cluster.ClusterData.addclustertitle)
				&& driver.findElement(addclusterpageheading).getText()
						.matches(Cluster.ClusterData.addclusterpageheadingText)) {
			Log.info("verified Add cluster page title and heading ");
			return true;
		} else {
			Log.error("Error during verifying add cluster page and title");
			return false;
		}
	}

	public void doAddCluster() throws InterruptedException {
		Log.info("Adding Cluster ..");
		driver.findElement(clusetername).sendKeys(
				Cluster.ClusterData.clusternameText);
		driver.findElement(clusterdes).sendKeys(
				Cluster.ClusterData.clusterdesText);
		driver.findElement(hospitalname).sendKeys(
				Cluster.ClusterData.hospitalnameText);
		driver.findElement(hospitalautosuggession).click();
		driver.findElement(search).sendKeys(Cluster.ClusterData.searchText);
		FrameWork.Click(driver, hospitalautosuggessionlist);
		driver.findElement(submit).click();
		Thread.sleep(2000);
		System.out.println("Title add after " + driver.getTitle());
		updaturl = driver.getCurrentUrl();
		System.out.println("Current page title " + driver.getCurrentUrl());
		Assert.assertEquals(driver.getTitle(), Cluster.ClusterData.Updatetitle);

	}

	public void doUpdateCluster() {
		driver.get(updaturl);
		Log.info("Updating Cluster ..");
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, clusetername,
				Cluster.ClusterData.clusternameText));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, clusterdes,
				Cluster.ClusterData.clusterdesText));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver,
				addedhospitalname, Cluster.ClusterData.hospitalnameText));
	}

	public void doBlankAllField() {
		Log.info("Verifying Error messages for mandatory fields");
		driver.findElement(clusetername).clear();
		driver.findElement(clusterdes).clear();
		driver.findElement(hospitalname).clear();
		driver.findElement(search).clear();
		driver.findElement(submit).click();
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				allblankfielderrormessage,
				Cluster.ClusterData.allblankfielderrormessageText));
	}


	
}
