package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.*;
import com.mycol.automation.factorydata.*;

public class SignInPage {

	private WebDriver driver;

	public By LoginUserName = By.id("inputEmail");
	private By LoginPassword = By.id("inputPassword");
	private By LoginSubmitButton = By
			.xpath(".//*[@id='signin']/div[4]/div[2]/button");

	private By LoginUserorEmailError = By
			.xpath(".//*[@id='signin']/div[1]/div/label");
	private By LoginPasswordError = By
			.xpath(".//*[@id='signin']/div[2]/div/label");
	private By LoginErrorNotAuth = By.xpath(".//*[@id='content']/div[1]");
	private By openLoginPage = By.linkText("Login");
	private By pageheading = By.className("form-signin-heading");

	private By usernameVerify = By
			.xpath("html/body/div[1]/div[1]/nav/ul[2]/li[3]/a");

	public SignInPage(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting SignInPage ... ");
	}

	public void Logout() {
		driver.get(Login.LoginData.logout);
	}

	public void doLogin() throws InterruptedException {
		Log.info("closing city popup");
		Library.popupCityChoose(driver);
		Log.info("Opening login page");
		openLoginPage();
		Log.info("Entering User name ");
		FrameWork.Type(driver, LoginUserName, Login.LoginData.username);
		Log.info("Entering Password");
		FrameWork.Type(driver, LoginPassword, Login.LoginData.password);
		Log.info("Clicking on signin button");
		FrameWork.Click(driver, LoginSubmitButton);
		Thread.sleep(3000);
		String user = FrameWork.GetText(driver, usernameVerify).replaceAll(
				" .*", "");
		Assert.assertEquals(user, Login.LoginData.usernameVerifyText);

	}

	public void clearLoginFields() {

		driver.findElement(LoginPassword).clear();
		driver.findElement(LoginUserName).clear();

	}

	public void blankUserNameNPassword() {

	
		clearLoginFields();
		driver.findElement(LoginSubmitButton).click();
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				LoginUserorEmailError, Login.LoginData.LoginErrorMessage));
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				LoginPasswordError, Login.LoginData.LoginErrorMessage));
	}

	public void validUserNameBlankPassword() {
		
		clearLoginFields();
		FrameWork.Type(driver, LoginUserName, Login.LoginData.username);
		FrameWork.Click(driver, LoginSubmitButton);
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				LoginPasswordError, Login.LoginData.LoginErrorMessage));
	}

	public void validPasswordeBlankUername() {

		clearLoginFields();
		FrameWork.Type(driver, LoginPassword, Login.LoginData.username);
		FrameWork.Click(driver, LoginSubmitButton);
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver,
				LoginUserorEmailError, Login.LoginData.LoginErrorMessage));
	}

	public void openLoginPage() {

		FrameWork.Click(driver, openLoginPage);
	}

	public void verifyPageHeading() {
		openLoginPage();
		Assert.assertEquals(driver.getTitle(), Login.LoginData.title);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,
				Login.LoginData.pageheading));
	}
	public void doLoginByMobile() throws InterruptedException {
		Logout();
		Log.info("closing city popup");
		Library.popupCityChoose(driver);
		Log.info("Opening login page");
		openLoginPage();
		Log.info("Entering User name ");
		FrameWork.Type(driver, LoginUserName, Login.LoginData.mobile);
		Log.info("Entering Password");
		FrameWork.Type(driver, LoginPassword, Login.LoginData.password);
		Log.info("Clicking on signin button");
		FrameWork.Click(driver, LoginSubmitButton);
		Thread.sleep(3000);
		String user = FrameWork.GetText(driver, usernameVerify).replaceAll(
				" .*", "");
		Assert.assertEquals(user, Login.LoginData.usernameVerifyText);

	}
	
	
}