package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalPayments {

	protected WebDriver driver;
	private By linkpatientdetails = By.linkText("(G) Payments");
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By cghsacceptence = By.id("Hospital_cghs_acceptence");
	// Charges for procedures
	private By dedicateinsurancehelp = By
			.id("Hospital_dedicated_insurance_helpdesk");
	private By chkkneereplacement = By.id("Hospital_procedure_1");
	private By chklivertransplant = By.id("Hospital_procedure_2");
	private By chkopenheartsurgery = By.id("Hospital_procedure_3_available");
	private By chkangioplasty = By.id("Hospital_procedure_4_available");
	private By chkconsultancy = By.id("Hospital_procedure_5_available");
	private By kneereplacementcharge = By.id("Hospital_procedure_1_rate");
	private By chklivertransplantcharge = By.id("Hospital_procedure_2_rate");
	private By chkopenheartsurgerycharge = By.id("Hospital_procedure_3_rate");
	private By chkangioplastycharge = By.id("Hospital_procedure_4_rate");
	private By chkconsultancycharge = By.id("Hospital_procedure_5_rate");
	// Insurance Tie Ups
	private String chkinsurancetieups = "Hospital_insurance_company_"; // Hospital_insurance_company_44
	private String chkthirdpartytransfer = "Hospital_tpa_"; // Hospital_tpa_27
	private By btnsubmit = By.xpath(".//*[@id='hospital-form-7']/div[6]/input");
	private By successnotification = By
			.xpath(".//*[@id='msg-hospital-form-7']");

	// private By openheart

	public AdminAddHospitalPayments(WebDriver driver) {

		this.driver = driver;
	}

	public void openAddHospitalPaymentsPage() {
		
		FrameWork.Click(driver, linkpatientdetails);
	}
	public void doAddPayment() {
		FrameWork.SelectByVisibleText(driver, cghsacceptence,
				Hospital.payments.cghsacceptenceText);
		FrameWork.SelectByVisibleText(driver, dedicateinsurancehelp,
				Hospital.payments.dedicateinsurancehelpText);
		FrameWork.Click(driver, chkkneereplacement);
		FrameWork.Type(driver, kneereplacementcharge,
				Hospital.payments.kneereplacementchargeText);
		FrameWork.clickOnGroupInputEle(driver, chkinsurancetieups, 0, 44);
		FrameWork.clickOnGroupInputEle(driver, chkthirdpartytransfer, 0, 27);
		FrameWork.Click(driver, btnsubmit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver,
				successnotification, Hospital.payments.successnotification));
	}
	public void verifyHospitalPaymentsPage() {

		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading,
				Hospital.payments.pageheadingText));

	}

	public void doVerifyAddPayment() {
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, cghsacceptence,
				Hospital.payments.cghsacceptenceText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver,
				dedicateinsurancehelp,
				Hospital.payments.dedicateinsurancehelpText));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, kneereplacementcharge, Hospital.payments.kneereplacementchargeText));
		FrameWork.verifyGroupInputEle(driver, chkinsurancetieups, 0, 44);

	}

}
