package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalAccommodation {
	
	protected WebDriver driver;
	
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By linkpage = By.xpath(".//*[@id='leftSubLinks1']/li[10]/a");
	private By addaccommdation = By.xpath(".//*[@id='add_nearby_btn']");
	private By name = By.id("HospitalNearbyAccommodation_name");
	private By noofbeds = By.id("HospitalNearbyAccommodation_number_of_beds");
	private By indicativetariff = By.id("HospitalNearbyAccommodation_indicative_tariff");
	private By contacctpersonname = By.id("HospitalNearbyAccommodation_contact_person_name_0");
	private By contactpersonno = By.id("HospitalNearbyAccommodation_contact_number_0");
	private By btnsubmit = By.xpath(".//*[@id='hospital-form-10']/div[5]/input");
	private By errormobilenoti = By.xpath(".//*[@id='nearby_row_0']/div[2]/div");
	private By errornamenoti = By.xpath(".//*[@id='hospital-form-10']/div[1]/div");
	
	private By lblname = By.xpath(".//*[@id='nearby_form']/table/tbody/tr[2]/td[1]");
	private By lblnofobeds = By.xpath(".//*[@id='nearby_form']/table/tbody/tr[2]/td[2]");
	private By lblindicativetarrif = By.xpath(".//*[@id='nearby_form']/table/tbody/tr[2]/td[3]");
	
	public AdminAddHospitalAccommodation(WebDriver driver)
	{ 
		this.driver=driver;
	}
	public void openAddHospitalAccommodationPage() {
		
		FrameWork.Click(driver, linkpage);
	}
	
	public void verifyAddHospitalAccommodationHeading()
	{
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading, Hospital.accommodation.pageheadingText));
	}
	
	public void doAddHospitalAccommodation()
	{
		FrameWork.Click(driver, addaccommdation);
		FrameWork.Type(driver, name,Hospital.accommodation.name);
		FrameWork.Type(driver, noofbeds,Hospital.accommodation.noofbeds);
		FrameWork.Type(driver, indicativetariff,Hospital.accommodation.indicativetariff);
		FrameWork.Type(driver, contacctpersonname,Hospital.accommodation.contacctpersonname);
		FrameWork.Type(driver, contactpersonno,Hospital.accommodation.contactpersonno);
		FrameWork.Click(driver, btnsubmit);
				
	}
	
	public void verifyHospitalAccommodation()
	{
		
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, lblname,Hospital.accommodation.name));
		/*Assert.assertTrue(FrameWork.verifySuccessMessage(driver, lblname,Hospital.accommodation.name));
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, lblnofobeds,Hospital.accommodation.noofbeds));
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, lblindicativetarrif,Hospital.accommodation.indicativetariff));*/
	}
}
