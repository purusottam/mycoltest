package com.mycol.automation.pagefactory;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;

public class AdminConciergeUserLeaveManagement {
	
	protected WebDriver driver;
	
	private By link_ConciergeManagement = By.linkText("Concierge Management");
	private By link_UserLeaveManagement = By.linkText("User Leave Management");
	private By txt_ManageUsersLeaves = By.xpath(".//*[@id='content']/h1");
	private String ManageUserLeavesTxt = "Manage Users Leaves";
	private By btn_ApplyLeave = By.xpath(".//*[@id='content']/a");
	private By txtField_User = By.id("UsersLeave_concierge");
	private String txt_UserField = "nirmal shama";
	private By txtField_StartDate = By.id("UsersLeave_start_date");
	private String txt_StartDateField = "29-04-2015";
	private By txtField_EndDate = By.id("UsersLeave_end_date");
	private String txt_EndDateField = "02-05-2015";
	private By dropdown_Status = By.id("UsersLeave_status");
	private String option_Applied = "Applied";
	private By txtField_Comment = By.id("UsersLeave_comment");
	private String txt_CommentField = "It will be approved.";
	private By btn_Create = By.xpath(".//*[@id='users-leave-form']/div[8]/input");
	
	
	public void verifyManageUsersLeavesPage(){
		FrameWork.Click(driver, link_ConciergeManagement);
		FrameWork.Click(driver, link_UserLeaveManagement);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, txt_ManageUsersLeaves, ManageUserLeavesTxt));
	}
	
	public void verifyApplyUsersLeaves(){
		FrameWork.Click(driver, link_ConciergeManagement);
		FrameWork.Click(driver, link_UserLeaveManagement);
		FrameWork.Click(driver, btn_ApplyLeave);
		FrameWork.Type(driver, txtField_User, txt_UserField);
		FrameWork.Type(driver, txtField_StartDate, txt_StartDateField);
		FrameWork.Type(driver, txtField_EndDate, txt_EndDateField);
		FrameWork.SelectByVisibleText(driver, dropdown_Status, option_Applied);
		FrameWork.Type(driver, txtField_Comment, txt_CommentField);
		FrameWork.Click(driver, btn_Create);
	}

}
