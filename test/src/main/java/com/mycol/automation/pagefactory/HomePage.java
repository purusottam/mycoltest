package com.mycol.automation.pagefactory;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.Library;
import com.auriga.automation.common.Log;

public class HomePage {
	protected WebDriver driver;
	String pagetitle = "Home Page";

	public HomePage(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("good name");
	}

	public void verifyBasePageTitle() throws InterruptedException{
		Library.popupCityChoose(driver);
		Log.info(driver.getTitle());
		Assert.assertEquals(driver.getTitle(), pagetitle);
	}
}