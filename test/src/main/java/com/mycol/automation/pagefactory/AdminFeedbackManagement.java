package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminFeedbackData;

public class AdminFeedbackManagement {
	protected WebDriver driver;
	public AdminFeedbackManagement(WebDriver driver)
	{
		this.driver=driver;
		Log.startTestCase("Starting Admin Feedback Management ... ");
	}

	private By replylink = By.xpath(".//*[@id='feedbacks-grid']/table/tbody/tr[1]/td[8]/a");
	private By replypopupheading = By.xpath(".//*[@id='ui-id-1']");
	private By feedback = By.xpath(".//*[@id='feedback']");
	private By btnreply = By.xpath(".//*[@id='reply_btn']");
	private By errfeedback = By.xpath(".//*[@id='feedback_err']");
	private By parentlink = By.linkText("Concierge Management");
	private By childlink = By.linkText("Feedback Management");
	private By msgsuccess = By.xpath(".//*[@id='content']/ul/li/div");
	
	public void doOpenFeedbackPage()
	{
	FrameWork.Click(driver, parentlink);
	FrameWork.Click(driver, childlink);
	Log.info("Feedback page has opened.");
	}
	
	public void doOpenFeedbackPopup()
	{
		FrameWork.Click(driver,replylink);
		Log.info("Verifying Feedback popup opened or not");
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, replypopupheading,AdminFeedbackData.replypopupheadingText));
		
	}
	
	public void doAddFeedback()
	{
		FrameWork.Type(driver, feedback, AdminFeedbackData.feedbackText);
		FrameWork.Click(driver, btnreply);
		Log.info("Adding Feedback");
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, msgsuccess, AdminFeedbackData.msgsuccessText));
	}
	
	public void doVerifyMandatoryField()
	{
		doOpenFeedbackPopup();
		FrameWork.Click(driver, btnreply);
		Log.info("Verifying Feedback comment box mandatory or not");
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver, errfeedback, AdminFeedbackData.errfeedbackText));
	}

}
