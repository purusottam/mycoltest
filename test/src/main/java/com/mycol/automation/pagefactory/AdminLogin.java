package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Adminlogin;

public class AdminLogin {

	protected WebDriver driver;

	private By username = By.id("UserLogin_username");
	private By password = By.id("UserLogin_password");
	private By submit = By.xpath(".//*[@id='content']/div/form/div[4]/input");
	private By success = By.xpath(".//*[@id='content']/h1");
	private By LoginUserError = By.xpath(".//*[@id='content']/div/form/div[1]/ul/li[1]");
	private By LoginPasswordError = By
			.xpath(".//*[@id='content']/div/form/div[1]/ul/li[2]");

	public AdminLogin(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting Admin Login ... ");
	}

	public void clearFields() {
		// openLoginPage();
		driver.findElement(username).clear();
		driver.findElement(password).clear();
		Log.info("Clearing fields");

	}

	public void doLogin() {
		Log.info("Doing loging");
		driver.findElement(username).sendKeys(
				Adminlogin.AdminloginData.usernameText);
		driver.findElement(password).sendKeys(
				Adminlogin.AdminloginData.passwordText);
		driver.findElement(submit).click();
	}

	public void openLoginPage() {
		driver.get("http://admin.staging.colhealth.org/");
	}

	public Boolean verifyLogin() {
		if (driver.findElement(success).isDisplayed()
				|| driver.findElement(submit).isSelected()
				&& driver.findElement(success).getText()
						.matches(Adminlogin.AdminloginData.successMessage)) {
			Log.info("Admin User logged in successfully");
			return true;
		} else {
			Log.error("Error - Admin User not logged in ");
			return false;
		}
	}

	public void logout() {
		driver.get(Adminlogin.AdminloginData.logout);
	}

	public Boolean blankUserNameNPassword() {

		clearFields();
		driver.findElement(submit).click();
		;
		/*WebDriverWait wait = new WebDriverWait(driver, 8);
		WebElement w = driver.findElement(LoginUserError);
		wait.until(ExpectedConditions.textToBePresentInElement(w,
				Adminlogin.AdminloginData.usernameErrorText));*/
		if (FrameWork.verifyErrorMessage(driver,LoginUserError,Adminlogin.AdminloginData.usernameErrorText)) {
			Log.info("blankUserNameNPassword "
					+ driver.findElement(LoginUserError).getText());
			return true;

		} else {
			Log.error("Error in blankUserNameNPassword ");
			return false;
		}

	}

}
