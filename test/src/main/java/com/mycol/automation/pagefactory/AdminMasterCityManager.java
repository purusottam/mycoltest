package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminmastercitymanagerPage;

public class AdminMasterCityManager {
	
	protected WebDriver driver;
	
	private By master = By.linkText("Masters");
	private By citymanagerlink = By.linkText("City Manager");
	private By addcitybutton = By.linkText("Add City");
	private By addcitypageheading = By.xpath(".//*[@id='content']/h1");
	private By grp_name = By.id("City_group_name_id");
	private By state = By.id("City_state_id");
	private By name = By.id("City_name");
	private By status = By.id("City_status");
	private By center_latitude = By.id("City_center_lat");
	private By center_longitude = By.id("City_center_lng");
	private By create = By.name("yt0");
	protected String update_url = "http://admin.staging.colhealth.org/index.php/cities/update/11";
	private By stateBlankfieldErrormessage = By.xpath(".//*[@id='city-form']/div[2]/div");
	private By nameBlankfieldErrormessage = By.xpath(".//*[@id='city-form']/div[3]/div");
	private By latitudeBlankfieldErrormessage = By.xpath(".//*[@id='city-form']/div[5]/div");
	private By longitudeBlankfieldErrormessage = By.xpath(".//*[@id='city-form']/div[6]/div");
	
	
	public AdminMasterCityManager(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting AdminMasterCityManager ....");
	}
	
	public Boolean doVerifyCityManagerPage() {

		Log.startTestCase("doVerifyCityManagerPage");
		if (driver.getTitle().matches(AdminmastercitymanagerPage.AdminmastercitymanagerData.title)) {
			Log.info("Pass doVerifyCityManagerPage");
			return true;
		} else {
			Log.error("Error in doVerifyCityManagerPage");
			return false;
		}
	}
	
	public void doOpenManageCityManagerPage() {
		Log.info("Opening doOpenManageCityManagerPage");
		//driver.findElement(master).click();
		FrameWork.Click(driver, master);
		//driver.findElement(citymanagerlink).click();
		FrameWork.Click(driver, citymanagerlink);
	}
	
	public void doOpenAddCityManagerPage() {
		Log.info("Opening doOpenAddCityManagerPage");
		//driver.findElement(addcitybutton).click();
		FrameWork.Click(driver, addcitybutton);
	}
	
	public Boolean doVerifyAddCityManagerPage() {
		if (driver.getTitle().matches(AdminmastercitymanagerPage.AdminmastercitymanagerData.addcitytitle)
				&& FrameWork.GetText(driver, addcitypageheading).matches(
						AdminmastercitymanagerPage.AdminmastercitymanagerData.addcitypageheadingText)) {
			Log.info("Pass doVerifyAddCityManagerPage");
			return true;
		} else {
			Log.error("Error in doVerifyAddCityManagerPage");
			return false;
		}
	}
	
	public void doAddCity() throws InterruptedException {
		Log.info("Adding doAddCity");
		
		//new Select(driver.findElement(grp_name))
				//.selectByVisibleText(AdminmastercitymanagerPage.AdminmastercitymanagerData.grp_nameText);
		FrameWork.SelectByVisibleText(driver, grp_name, AdminmastercitymanagerPage.AdminmastercitymanagerData.grp_nameText);
		
		//new Select(driver.findElement(state))
				//.selectByVisibleText(AdminmastercitymanagerPage.AdminmastercitymanagerData.stateText);
		FrameWork.SelectByVisibleText(driver, state, AdminmastercitymanagerPage.AdminmastercitymanagerData.stateText);
		
		//driver.findElement(name).clear();
		//driver.findElement(name).sendKeys(AdminmastercitymanagerPage.AdminmastercitymanagerData.nameText);
		FrameWork.Type(driver, name, AdminmastercitymanagerPage.AdminmastercitymanagerData.nameText);
		
		//new Select(driver.findElement(status))
				//.selectByVisibleText(AdminmastercitymanagerPage.AdminmastercitymanagerData.statusText);
		FrameWork.SelectByVisibleText(driver, status, AdminmastercitymanagerPage.AdminmastercitymanagerData.statusText);
		
		//driver.findElement(center_latitude).clear();
		//driver.findElement(center_latitude).sendKeys(
				//AdminmastercitymanagerPage.AdminmastercitymanagerData.center_latitudeText);
		FrameWork.Type(driver, center_latitude, AdminmastercitymanagerPage.AdminmastercitymanagerData.center_latitudeText);
		
		//driver.findElement(center_longitude).clear();
		//driver.findElement(center_longitude).sendKeys(
				//AdminmastercitymanagerPage.AdminmastercitymanagerData.center_longitudeText);
		FrameWork.Type(driver, center_longitude, AdminmastercitymanagerPage.AdminmastercitymanagerData.center_longitudeText);
		
		//driver.findElement(create).click();
        FrameWork.Click(driver, create);
		Thread.sleep(3000);
		String successPageTitle = "My Col - Admin Cities";
		//FrameWork.verifySuccessMessage(driver, successpageTitle, successPageTitle);
		Assert.assertTrue(driver.getTitle().matches(successPageTitle),"Page title did not match");
	}
	
	public void doUpdateCity() throws InterruptedException {
		driver.get(update_url);
		doAddCity();
		driver.get(update_url);
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, name,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.nameText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, name,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.statusText));
	}
	
	public void doAddCityBlankField() throws InterruptedException {
		doOpenManageCityManagerPage();
		doOpenAddCityManagerPage();
		
		//new Select(driver.findElement(state))
				//.selectByVisibleText(AdminmastercitymanagerPage.AdminmastercitymanagerData.blankStateText);
		FrameWork.SelectByVisibleText(driver, state, AdminmastercitymanagerPage.AdminmastercitymanagerData.blankStateText);
		
		driver.findElement(name).clear();
		driver.findElement(center_latitude).clear();
		driver.findElement(center_longitude).clear();
		driver.findElement(create).click();
		
		if(FrameWork.verifyErrorMessage(driver, stateBlankfieldErrormessage,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.stateBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, nameBlankfieldErrormessage,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.nameBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, latitudeBlankfieldErrormessage,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.latitudeBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, longitudeBlankfieldErrormessage,
				AdminmastercitymanagerPage.AdminmastercitymanagerData.longitudeBlankfieldErrormessageText)){
			Log.info("Pass doAddCityBlankField");
			} else {
				Log.error("Error in doAddCityBlankField");
		}
	}

}
