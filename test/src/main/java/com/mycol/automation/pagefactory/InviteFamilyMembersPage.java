package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.factorydata.Invitefamilymemberspage;


public class InviteFamilyMembersPage extends TestBaseSetup {
	protected WebDriver driver;
	private By welcome = By.partialLinkText("Welcome");
	private By mycircle = By
			.linkText("My Circle");
	private By gender = By.xpath(".//*[@id='invite']/div[2]/div/div[1]/input");
	private By mobile = By.name("mobile");
	private By firstname = By.name("firstname");
	private By lastname = By.name("lastname");
	private By inviteButton = By.id("inviteBtn");
	private By pageHeading = By.xpath(".//*[@id='invite']/h2");
	private By success = By.xpath(".//*[@id='content']/div[1]");
	private By relation = By.id("relation");

	public InviteFamilyMembersPage(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyPageHeading() {
	Log.info("Veriying page heading of invite family member");
	Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageHeading, Invitefamilymemberspage.InvitefamilymemberspageData.pageheadingtext));
			
	}

	public void openInviteFamilyMembersPage() {
		
		Log.info("Inviting family members ");
		FrameWork.Click(driver, welcome);
		FrameWork.Click(driver, mycircle);
	
	}

	public void doInvite() {
		Log.info("Adding Realation ..");
		FrameWork.SelectByVisibleText(driver,relation, Invitefamilymemberspage.InvitefamilymemberspageData.relation );
		FrameWork.Click(driver, gender);
		FrameWork.Type(driver, mobile, Invitefamilymemberspage.InvitefamilymemberspageData.mobiletext);
		FrameWork.Type(driver, firstname, Invitefamilymemberspage.InvitefamilymemberspageData.firstnametext);
		FrameWork.Type(driver, lastname, Invitefamilymemberspage.InvitefamilymemberspageData.lastnametext);
		FrameWork.Click(driver, inviteButton);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, success, Invitefamilymemberspage.InvitefamilymemberspageData.successmessage),"Relation is not added");

	}
	
	
	

}
