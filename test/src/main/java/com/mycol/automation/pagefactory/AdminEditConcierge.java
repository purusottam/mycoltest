package com.mycol.automation.pagefactory;

import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Admineditconcierge;
import com.mycol.automation.pagefactory.AdminAddConcierge;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

public class AdminEditConcierge {
	protected WebDriver driver;
	private By honorific = By.id("Concierge_honorific");
	private By addconciergePageHeading = By.xpath(".//*[@id='content']/h1");
	private By firstname = By.id("Concierge_first_name");
	private By lastname = By.id("Concierge_last_name");
	private By startdate = By.id("Concierge_start_date");
	private By dobdate = By.id("Concierge_dob");
	private By mobile = By.id("Concierge_mobile");
	private By email = By.id("Concierge_email");
	private By localaddress = By.id("Concierge_local_address");
	private By localarea = By.id("Concierge_local_area");
	private By localstate = By.id("Concierge_local_state");
	private By localcity = By.id("Concierge_local_city");
	private By localpincode = By.id("Concierge_local_pincode");
	private By latitude = By.id("Concierge_lat");
	private By longitude = By.id("Concierge_lng");
	private By permanentaddress = By.id("Concierge_permanent_address");
	private By permanentarea = By.id("Concierge_permanent_area");
	private By permanentstate = By.id("Concierge_permanent_state");
	private By permanentcity = By.id("Concierge_permanent_city");
	private By permanentpincode = By.id("Concierge_permanent_pincode");
	private By nativeplace = By.id("Concierge_native_place");
	private By educationalqualification = By
			.id("Concierge_educational_qualification");
	private By language = By.id("Concierge_language_spoken_0");
	private By language1 = By.id("Concierge_language_spoken_1");
	private By addmore = By
			.xpath(".//*[@id='concierge-form']/div[20]/input[1]");
	private By passport = By.xpath(".//*[@id='Concierge_document_0']");
	private By voterid = By.xpath(".//*[@id='Concierge_document_1']");
	private By submit = By.xpath(".//*[@id='concierge-form']/div[23]/input");

	// image
	private By image = By.id("concierge_image_file_field");
	private By passportimage = By.id("document_type_file_field");
	private By voteridimage = By.xpath(".//*[@id='voter_browse_btn']");
	private By permanentaddressimage = By
			.id("permanent_address_doc_file_field");

	private By imageupdate = By
			.xpath(".//*[@id='display_concierge_image']/img");
	private By docimage = By
			.xpath(".//*[@id='concierge_permanent_address_doc_container_1']/img");

	public AdminEditConcierge(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Admin Edit Concierge ");
	}
	// Update page title
	private String updatecontitle = "My Col - Update Concierge";

	@Test
	public void doFindUpdateUrl() {
		
		if (driver.getTitle().matches(updatecontitle)) {
		//	String currentUrl = driver.getCurrentUrl();
		//	System.out.println(currentUrl);
			Log.info("Verified upate url ");
		}

	}

	@Test
	public void doEditConcierge() {

		// driver.get("http://admin.staging.colhealth.org/index.php/concierge/update/17");

		new Select(driver.findElement(honorific)).selectByIndex(1);
		// String first = driver.findElement(firstname).getAttribute("values");
		driver.findElement(firstname).clear();
		driver.findElement(firstname).sendKeys(Admineditconcierge.AdmineditconciergeData.firstnameText);
		driver.findElement(lastname).clear();
		driver.findElement(lastname).sendKeys(Admineditconcierge.AdmineditconciergeData.lastnameText);
		driver.findElement(startdate).clear();
		driver.findElement(startdate).sendKeys(Admineditconcierge.AdmineditconciergeData.startdateText);
		driver.findElement(dobdate).clear();
		driver.findElement(dobdate).sendKeys(Admineditconcierge.AdmineditconciergeData.dobText);
		driver.findElement(mobile).clear();
		driver.findElement(mobile).sendKeys(Admineditconcierge.AdmineditconciergeData.mobileText);
		driver.findElement(email).clear();
		driver.findElement(email).sendKeys(Admineditconcierge.AdmineditconciergeData.emailText);
		driver.findElement(localaddress).clear();
		driver.findElement(localaddress).sendKeys(Admineditconcierge.AdmineditconciergeData.localaddressText);
		driver.findElement(localarea).clear();
		driver.findElement(localarea).sendKeys(Admineditconcierge.AdmineditconciergeData.localareaText);
		new Select(driver.findElement(localstate))
				.selectByValue(Admineditconcierge.AdmineditconciergeData.localstateText);
		if (driver.findElement(localcity).isEnabled()
				|| driver.findElement(localcity).isDisplayed())
			new Select(driver.findElement(localcity))
					.selectByValue(Admineditconcierge.AdmineditconciergeData.localcityText);
		driver.findElement(localpincode).clear();
		driver.findElement(localpincode).sendKeys(Admineditconcierge.AdmineditconciergeData.localpincodeText);
		driver.findElement(latitude).clear();
		driver.findElement(latitude).sendKeys(Admineditconcierge.AdmineditconciergeData.latitudeText);
		driver.findElement(longitude).clear();
		driver.findElement(longitude).sendKeys(Admineditconcierge.AdmineditconciergeData.longitudeText);
		driver.findElement(permanentaddress).clear();
		driver.findElement(permanentaddress).sendKeys(Admineditconcierge.AdmineditconciergeData.permanentaddressText);
		driver.findElement(permanentarea).clear();
		driver.findElement(permanentarea).sendKeys(Admineditconcierge.AdmineditconciergeData.permanentareaText);

		new Select(driver.findElement(permanentstate))
				.selectByValue(Admineditconcierge.AdmineditconciergeData.permanentstateText);
		if (driver.findElement(permanentcity).isDisplayed()
				|| driver.findElement(permanentcity).isEnabled())
			new Select(driver.findElement(permanentcity))
					.selectByValue(Admineditconcierge.AdmineditconciergeData.permanentcityText);
		driver.findElement(permanentpincode).clear();
		driver.findElement(permanentpincode).sendKeys(Admineditconcierge.AdmineditconciergeData.permanentpincodeText);
		driver.findElement(nativeplace).clear();
		driver.findElement(nativeplace).sendKeys(Admineditconcierge.AdmineditconciergeData.nativeplaceText);
		driver.findElement(educationalqualification).clear();
		driver.findElement(educationalqualification).sendKeys(
				Admineditconcierge.AdmineditconciergeData.educationalqualificationText);
		driver.findElement(language).clear();
		driver.findElement(language).sendKeys(Admineditconcierge.AdmineditconciergeData.language1Text);
		driver.findElement(language1).clear();
		driver.findElement(language1).sendKeys(Admineditconcierge.AdmineditconciergeData.language2Text);
		driver.findElement(submit).click();
		Log.info("Editing Concierge page ");
		// Thread.sleep(28000);
		/*
		 * driver.findElement(image).click();
		 * driver.findElement(image).sendKeys(imageLink); //
		 * driver.findElement(passportimage).click();
		 * driver.findElement(passportimage).sendKeys(passportimageLink); //
		 * driver.findElement(voteridimage).click();
		 * driver.findElement(voteridimage).sendKeys(voteridimageLink); //
		 * driver.findElement(permanentaddressimage).click();
		 * driver.findElement(permanentaddressimage).sendKeys(
		 * permanentaddressimageLink);
		 */

	}

	@Test
	public Boolean doVerifyAddConcierge() {

		// driver.get("http://admin.staging.colhealth.org/index.php/concierge/update/17");

		if (!driver.findElement(firstname).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.firstnameText)) {

			Log.info(driver.findElement(firstname).getAttribute(
					"value")
					+ "First name is not pass");
			return false;

		}
		if (!driver.findElement(lastname).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.lastnameText)) {
			Log.info("Last name is not passed");
			return false;
		}

		if (!driver.findElement(startdate).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.startdateText)) {
			Log.info("startdate is not passed");
			return false;
		}
		if (!driver.findElement(dobdate).getAttribute("value").matches(Admineditconcierge.AdmineditconciergeData.dobText)) {
			Log.info("dobdate is not passed");
			return false;
		}
		if (!driver.findElement(mobile).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.mobileText)) {
			Log.info("mobile is not passed");
			return false;
		}
		if (!driver.findElement(email).getAttribute("value").matches(Admineditconcierge.AdmineditconciergeData.emailText)) {
			Log.info("email is not passed");
			return false;
		}
		if (!driver.findElement(localaddress).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.localaddressText)) {
			Log.info("localaddress is not passed");
			return false;
		}
		if (!driver.findElement(localarea).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.localareaText)) {
			Log.info("localarea is not passed");
			return false;
		}

		// Dropdown values local state and city
		if (!driver.findElement(localstate).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.localstateText)) {
			Log.info("localstate is not passed");
			return false;
		}
		if (!driver.findElement(localcity).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.localcityText)) {
			Log.info("localcity is not passed");
			return false;
		}
		if (!driver.findElement(localpincode).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.localpincodeText)) {
			Log.info("localpincode is not passed");
			return false;
		}
		if (!driver.findElement(latitude).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.latitudeText)) {
			Log.info("latitude is not passed");
			return false;
		}
		if (!driver.findElement(longitude).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.longitudeText)) {
			Log.info("longitude is not passed");
			return false;
		}
		if (!driver.findElement(permanentaddress).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.permanentaddressText)) {
			Log.info("permanentaddress is not passed");
			return false;
		}
		if (!driver.findElement(permanentarea).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.permanentareaText)) {
			Log.info("permanentarea is not passed");
			return false;
		}

		// Dropdown values permanent state and city

		if (!driver.findElement(permanentstate).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.permanentstateText)) {
			Log.info("permanentstate is not passed");
			return false;
		}

		if (!driver.findElement(permanentcity).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.permanentcityText)) {
			Log.info("permanentcity is not passed");
			return false;
		}

		if (!driver.findElement(permanentpincode).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.permanentpincodeText)) {
			Log.info("permanentpincode is not passed");
			return false;
		}

		if (!driver.findElement(nativeplace).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.nativeplaceText)) {
			Log.info("nativeplace is not passed");
			return false;
		}

		if (!driver.findElement(educationalqualification).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.educationalqualificationText)) {
			Log.info("educationalqualification is not passed");
			return false;
		}

		if (!driver.findElement(language).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.language1Text)) {
			Log.info("language is not passed");
			return false;
		}

		if (!driver.findElement(language1).getAttribute("value")
				.matches(Admineditconcierge.AdmineditconciergeData.language2Text)) {
			Log.info("language1 is not passed");
			return false;
		}

		/*
		 * if (driver.findElement(imageupdate).getAttribute("src").matches(""))
		 * { Log.info("imageupdate is not passed"); return false; }
		 * 
		 * if (driver.findElement(docimage).getAttribute("src").matches("")) {
		 * Log.info("docimage  is not passed"); return false; }
		 */

		return true;

	}

}
