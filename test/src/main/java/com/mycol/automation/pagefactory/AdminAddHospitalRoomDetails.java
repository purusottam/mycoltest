package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalRoomDetails {
	protected WebDriver driver;
	private By pageheading = By.xpath(".//*[@id='content']/h1");
	private By linkroomdetail = By.linkText("(B) Room Details");
	private By bedcount = By.id("Hospital_bed_count");
	private By suitesingle = By.id("Hospital_1_room_type_bed");
	private By suitesharing = By.id("Hospital_2_room_type_bed");
	private By deleuxesingle = By.id("Hospital_3_room_type_bed");
	private By deleuxesharing = By.id("Hospital_4_room_type_bed");
	private By genralsingle = By.id("Hospital_5_room_type_bed");
	private By genralsharing = By.id("Hospital_6_room_type_bed");
	private By genralward = By.id("Hospital_7_room_type_bed");
	private By bedreserved = By.id("Hospital_ews_bed_reserved");
	private By btnsubmit = By.xpath(".//*[@id='hospital-form-2']/div[4]/input");
	private By msgsuccess = By.xpath(".//*[@id='msg-hospital-form-2']");

	public AdminAddHospitalRoomDetails(WebDriver driver) {
		this.driver = driver;
	}

	public void openRoomDetailPage() {
	//	driver.findElement(linkroomdetail).click();
		FrameWork.Click(driver, linkroomdetail);
	}

	public Boolean doAddRoomHospital() {
		Log.info("Adding Room in  hospital..");
		driver.findElement(bedcount)
				.sendKeys(Hospital.roomdetails.bedcountText);
		driver.findElement(suitesingle).sendKeys(
				Hospital.roomdetails.suitesingleText);
		driver.findElement(suitesharing).sendKeys(
				Hospital.roomdetails.suitesharingText);
		driver.findElement(deleuxesingle).sendKeys(
				Hospital.roomdetails.deleuxesingleText);
		driver.findElement(deleuxesharing).sendKeys(
				Hospital.roomdetails.deleuxesharingText);
		driver.findElement(genralsharing).sendKeys(
				Hospital.roomdetails.genralsharingText);
		driver.findElement(genralsingle).sendKeys(
				Hospital.roomdetails.genralsingleText);
		driver.findElement(genralward).sendKeys(
				Hospital.roomdetails.genralwardText);
		driver.findElement(bedreserved).sendKeys(
				Hospital.roomdetails.bedreservedText);

		FrameWork.Click(driver, btnsubmit);

		if (FrameWork.verifySuccessMessage(driver, msgsuccess,
				Hospital.roomdetails.msgsuccessText)) {
			Log.info("Room in Hospital Added Successfully ");
			return true;

		} else {
			Log.info("Room in Hospital Not Added Successfully ");
			return false;

		}

	}

	public void verifyAddedRoomHospital() {

		Assert.assertTrue(FrameWork.verifInputFieldData(driver, bedcount,Hospital.roomdetails.bedcountText),"Bedcount field is not verified");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver, suitesingle,
				Hospital.roomdetails.suitesingleText),"suitesingle field is not verified");

		Assert.assertTrue(FrameWork.verifInputFieldData(driver, suitesharing,
				Hospital.roomdetails.suitesharingText),"suitesingle field is not verified");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, deleuxesingle,
				Hospital.roomdetails.deleuxesingleText),"deleuxesingle field is not verified");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, deleuxesharing,
				Hospital.roomdetails.deleuxesharingText),"deleuxesharing field is not verified");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, genralsharing,
				Hospital.roomdetails.genralsharingText),"genralsharing field is not verified");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, genralsingle,
				Hospital.roomdetails.genralsingleText),"genralsingle field is not verified");
		
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, genralward,
				Hospital.roomdetails.genralwardText),"genralward field is not verified");
		

		Assert.assertTrue(FrameWork.verifInputFieldData(driver, bedreserved,
				Hospital.roomdetails.bedreservedText),"bedreserved field is not verified");
		

	}

	public Boolean verifyRoomDetailPage() {
		return FrameWork.verifySuccessMessage(driver, pageheading,
				Hospital.roomdetails.pageheadingText);

	}
	
	public void clearAllField()
	{
driver.findElement(bedcount).clear();
driver.findElement(suitesingle).clear();
driver.findElement(suitesharing).clear();
driver.findElement(deleuxesingle).clear();
driver.findElement(deleuxesharing).clear();
driver.findElement(genralsharing).clear();
driver.findElement(genralsingle).clear();
driver.findElement(genralward).clear();
driver.findElement(bedreserved).clear();
	}

}
