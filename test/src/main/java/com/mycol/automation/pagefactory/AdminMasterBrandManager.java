package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminmasterbrandmanagerPage;

public class AdminMasterBrandManager {

	protected WebDriver driver;

	private By master = By.linkText("Masters");
	private By brandmanagerlink = By.linkText("Brand Manager");
	private By add_Brandbutton = By.linkText("Add Brand");
	private By brand_name = By.id("Brand_brand");
	private By status = By.id("Brand_status");
	private By successnotification = By.xpath(".//*[@id='content']/ul/li/div");
	private By create = By.name("yt0");
	private By addbrandpageheading = By.xpath(".//*[@id='content']/h1");
	protected String updaturl = "http://admin.staging.colhealth.org/index.php/brands/update/1";
	private By brandBlankfieldErrormessage = By.xpath(".//*[@id='brand-form']/div[2]/div");

	
	public AdminMasterBrandManager(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting AdminMasterCityGroups ....");
	}
	
	public Boolean doVerifyBrandManagerPage() {

		Log.startTestCase("doVerifyBrandManagerPage");
		if (driver.getTitle().matches(AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.title)) {
			Log.info("Pass doVerifyBrandManagerPage");
			return true;
		} else {
			Log.error("Error in doVerifyBrandManagerPage");
			return false;
		}
	}
	
	public void doOpenManageBrandsPage() {
		Log.info("Opening doOpenManageBrandsPage");
		FrameWork.Click(driver, master);
		FrameWork.Click(driver, brandmanagerlink);
	}
	
	public void doOpenAddBrandPage() {
		Log.info("Opening doOpenAddCityGroupPage");
		FrameWork.Click(driver, add_Brandbutton);
	}
	
	public Boolean doVerifyAddBrandPage() {
		if (driver.getTitle().matches(AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.addbrandtitle)
				&& FrameWork.GetText(driver, addbrandpageheading).matches(
						AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.addbrandpageheadingText)) {
			Log.info("Pass doVerifyAddBrandPage");
			return true;
		} else {
			Log.error("Error in doVerifyAddBrandPage");
			return false;
		}
	}
	
	public void doAddBrand() throws InterruptedException {
		Log.info("Adding doAddBrand");
		FrameWork.Type(
				driver, brand_name, AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.brand_nameText);
		
		FrameWork.SelectByVisibleText(
				driver, status, AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.statusText);
		
		FrameWork.Click(driver, create);
		Thread.sleep(3000);
		String successNotification = "Brand "
				+ AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.brand_nameText
				+ " created Successfully";
		FrameWork.verifySuccessMessage(driver, successnotification,	successNotification);

	}
	
	public void doUpdateBrand() throws InterruptedException {
		driver.get(updaturl);
		doAddBrand();
		driver.get(updaturl);
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, brand_name,
				AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.brand_nameText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, status,
				AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.statusText));
	}

	public void doAddBrandBlankField() throws InterruptedException {
		doOpenManageBrandsPage();
		doOpenAddBrandPage();
		
		driver.findElement(brand_name).clear();
		FrameWork.Click(driver, create);
		
		Assert.assertTrue(FrameWork.verifyErrorMessage(driver, brandBlankfieldErrormessage,
					AdminmasterbrandmanagerPage.AdminmasterbrandmanagerData.brandBlankfieldErrormessageText));
	}
	
}