package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

import com.mycol.automation.factorydata.Concierge;
import com.auriga.automation.common.*;

public class AdminAddConcierge {

	private By pageheading = By.xpath(".//*[@id='content']/h1");

	private By addConciergeButton = By.xpath(".//*[@id='content']/a");

	// Add Concierge page
	private By honorific = By.id("Concierge_honorific");
	private By addconciergePageHeading = By.xpath(".//*[@id='content']/h1");
	private By firstname = By.id("Concierge_first_name");
	private By lastname = By.id("Concierge_last_name");
	private By startdate = By.id("Concierge_start_date");
	private By dobdate = By.id("Concierge_dob");
	private By mobile = By.id("Concierge_mobile");
	private By email = By.id("Concierge_email");
	private By localaddress = By.id("Concierge_local_address");
	private By localarea = By.id("Concierge_local_area");
	private By localstate = By.id("Concierge_local_state");
	private By localcity = By.id("Concierge_local_city");
	private By localpincode = By.id("Concierge_local_pincode");
	private By latitude = By.id("Concierge_lat");
	private By longitude = By.id("Concierge_lng");
	private By permanentaddress = By.id("Concierge_permanent_address");
	private By permanentarea = By.id("Concierge_permanent_area");
	private By permanentstate = By.id("Concierge_permanent_state");
	private By permanentcity = By.id("Concierge_permanent_city");
	private By permanentpincode = By.id("Concierge_permanent_pincode");
	private By nativeplace = By.id("Concierge_native_place");
	private By educationalqualification = By
			.id("Concierge_educational_qualification");
	private By language = By.id("Concierge_language_spoken_0");
	private By language1 = By.id("Concierge_language_spoken_1");
	private By addmore = By
			.xpath(".//*[@id='concierge-form']/div[20]/input[1]");
	private By passport = By.xpath(".//*[@id='Concierge_document_0']");
	private By voterid = By.xpath(".//*[@id='Concierge_document_1']");
	private By submit = By.xpath(".//*[@id='concierge-form']/div[22]/input");

	// image
	private By image = By.id("concierge_image_file_field");
	private By passportimage = By.id("document_type_file_field");
	private By voteridimage = By.xpath(".//*[@id='voter_browse_btn']");
	private By permanentaddressimage = By
			.id("permanent_address_doc_file_field");

	private By imageupdate = By
			.xpath(".//*[@id='display_concierge_image']/img");
	private By docimage = By
			.xpath(".//*[@id='concierge_permanent_address_doc_container_1']/img");

	
	protected WebDriver driver;


	public AdminAddConcierge(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Admin Add Concierge page test starting ");
	}


	public void openConcierges() {
		driver.get(Concierge.ConciergeData.conpagelink);
		Log.info("opening Concierge");
	}

	
	public Boolean doVerifyPageTitle() {
		if (driver.getTitle().matches(Concierge.ConciergeData.pageTitle))
		{
			Log.info("Verified page title");
			return true;
		}
		else
		{
			Log.error("Error in page title");
			return false;
		}
	}

	
	public Boolean doVerifyPageHeading() {
		if (driver.findElement(pageheading).getText().matches(Concierge.ConciergeData.pageheadingText))
		{	
			Log.info("Verified page heading ");
			return true;
		}
		else
		{
			Log.error("Error in page heading ");
			return false;
		}
	}

	
	public void openAddCon() {
		driver.findElement(addConciergeButton).click();
		Log.info("Opening Add concierge page ");
	}

	
	public Boolean verifyAddConPageHeading() {
		if (driver.findElement(addconciergePageHeading).getText()
				.matches(Concierge.ConciergeData.addconciergePageHeadingText))
			return true;
		else
			return false;
	}

	
	public Boolean doFindUpdateUrl() {
		if (driver.getTitle().matches(Concierge.ConciergeData.updatecontitle)) {
			//String currentUrl = driver.getCurrentUrl();
			//System.out.println("Current url of update : " + currentUrl);
			Log.info("Verified updated url");
			return true;
		}
		else
		{
			Log.error("Error in Find updated url");
			return false;
		}
	
	}

	
	public void doAddConcierge() {
		new Select(driver.findElement(honorific)).selectByIndex(2);
		driver.findElement(firstname).sendKeys(Concierge.ConciergeData.firstnameText);
		driver.findElement(lastname).sendKeys(Concierge.ConciergeData.lastnameText);
		driver.findElement(startdate).sendKeys(Concierge.ConciergeData.startdateText);
		driver.findElement(dobdate).sendKeys(Concierge.ConciergeData.dobText);
		driver.findElement(mobile).sendKeys(Concierge.ConciergeData.mobileText);
		driver.findElement(email).sendKeys(Concierge.ConciergeData.emailText);
		driver.findElement(localaddress).sendKeys(Concierge.ConciergeData.localaddressText);
		driver.findElement(localarea).sendKeys(Concierge.ConciergeData.localareaText);
		new Select(driver.findElement(localstate))
				.selectByValue(Concierge.ConciergeData.localstateText);
		if (driver.findElement(localcity).isEnabled()
				|| driver.findElement(localcity).isDisplayed())
			new Select(driver.findElement(localcity))
					.selectByValue(Concierge.ConciergeData.localcityText);

		driver.findElement(localpincode).sendKeys(Concierge.ConciergeData.localpincodeText);
		driver.findElement(latitude).sendKeys(Concierge.ConciergeData.latitudeText);
		driver.findElement(longitude).sendKeys(Concierge.ConciergeData.longitudeText);
		driver.findElement(permanentaddress).sendKeys(Concierge.ConciergeData.permanentaddressText);
		driver.findElement(permanentarea).sendKeys(Concierge.ConciergeData.permanentareaText);

		new Select(driver.findElement(permanentstate))
				.selectByValue(Concierge.ConciergeData.permanentstateText);
		if (driver.findElement(permanentcity).isDisplayed()
				|| driver.findElement(permanentcity).isEnabled())
			new Select(driver.findElement(permanentcity))
					.selectByValue(Concierge.ConciergeData.permanentcityText);

		driver.findElement(permanentpincode).sendKeys(Concierge.ConciergeData.permanentpincodeText);
		driver.findElement(nativeplace).sendKeys(Concierge.ConciergeData.nativeplaceText);
		driver.findElement(educationalqualification).sendKeys(
				Concierge.ConciergeData.educationalqualificationText);

		driver.findElement(language).sendKeys(Concierge.ConciergeData.language1Text);
		if (driver.findElement(addmore).isDisplayed()
				|| driver.findElement(addmore).isEnabled())
			driver.findElement(addmore).click();
		driver.findElement(language1).sendKeys(Concierge.ConciergeData.language2Text);

		// if(! driver.findElement(passport).isSelected())
		driver.findElement(passport).click();
		// if(! driver.findElement(voterid).isSelected())
		driver.findElement(voterid).click();

		//driver.findElement(image).click();
		driver.findElement(image).sendKeys(Concierge.ConciergeData.imageLink);
		// driver.findElement(passportimage).click();
		driver.findElement(passportimage).sendKeys(Concierge.ConciergeData.passportimageLink);
		// driver.findElement(voteridimage).click();
		driver.findElement(voteridimage).sendKeys(Concierge.ConciergeData.voteridimageLink);
		// driver.findElement(permanentaddressimage).click();
		driver.findElement(permanentaddressimage).sendKeys(
				Concierge.ConciergeData.permanentaddressimageLink);

		driver.findElement(submit).click();

		
		/*
		 * WebDriverWait wait1 = new WebDriverWait(driver, 30); WebElement w1 =
		 * driver.findElement(addConciergeButton);
		 * wait1.until(ExpectedConditions.elementToBeClickable(w1));
		 */
		
		Log.info("Adding Concierge");
	}

	
	public Boolean doVerifyAddConcierge() {

		// driver.get("http://admin.staging.colhealth.org/index.php/concierge/update/17");

		if (!driver.findElement(firstname).getAttribute("value")
				.matches(Concierge.ConciergeData.firstnameText)) {

			Log.info(driver.findElement(firstname).getAttribute(
					"value")
					+ "First name is not pass");
			return false;

		}
		if (!driver.findElement(lastname).getAttribute("value")
				.matches(Concierge.ConciergeData.lastnameText)) {
			Log.info("Last name is not passed");
			return false;
		}

		if (!driver.findElement(startdate).getAttribute("value")
				.matches(Concierge.ConciergeData.startdateText)) {
			Log.info("startdate is not passed");
			return false;
		}
		if (!driver.findElement(dobdate).getAttribute("value").matches(Concierge.ConciergeData.dobText)) {
			Log.info("dobdate is not passed");
			return false;
		}
		if (!driver.findElement(mobile).getAttribute("value")
				.matches(Concierge.ConciergeData.mobileText)) {
			Log.info("mobile is not passed");
			return false;
		}
		if (!driver.findElement(email).getAttribute("value").matches(Concierge.ConciergeData.emailText)) {
			Log.info("email is not passed");
			return false;
		}
		if (!driver.findElement(localaddress).getAttribute("value")
				.matches(Concierge.ConciergeData.localaddressText)) {
			Log.info("localaddress is not passed");
			return false;
		}
		if (!driver.findElement(localarea).getAttribute("value")
				.matches(Concierge.ConciergeData.localareaText)) {
			Log.info("localarea is not passed");
			return false;
		}

		// Dropdown values local state and city
		if (!driver.findElement(localstate).getAttribute("value")
				.matches(Concierge.ConciergeData.localstateText)) {
			Log.info("localstate is not passed");
			return false;
		}
		if (!driver.findElement(localcity).getAttribute("value")
				.matches(Concierge.ConciergeData.localcityText)) {
			Log.info("localcity is not passed");
			return false;
		}
		if (!driver.findElement(localpincode).getAttribute("value")
				.matches(Concierge.ConciergeData.localpincodeText)) {
			Log.info("localpincode is not passed");
			return false;
		}
		if (!driver.findElement(latitude).getAttribute("value")
				.matches(Concierge.ConciergeData.latitudeText)) {
			Log.info("latitude is not passed");
			return false;
		}
		if (!driver.findElement(longitude).getAttribute("value")
				.matches(Concierge.ConciergeData.longitudeText)) {
			Log.info("longitude is not passed");
			return false;
		}
		if (!driver.findElement(permanentaddress).getAttribute("value")
				.matches(Concierge.ConciergeData.permanentaddressText)) {
			Log.info("permanentaddress is not passed");
			return false;
		}
		if (!driver.findElement(permanentarea).getAttribute("value")
				.matches(Concierge.ConciergeData.permanentareaText)) {
			Log.info("permanentarea is not passed");
			return false;
		}

		// Dropdown values permanent state and city

		if (!driver.findElement(permanentstate).getAttribute("value")
				.matches(Concierge.ConciergeData.permanentstateText)) {
			Log.info("permanentarea is not passed");
			return false;
		}

		if (!driver.findElement(permanentcity).getAttribute("value")
				.matches(Concierge.ConciergeData.permanentcityText)) {
			Log.info("permanentarea is not passed");
			return false;
		}

		if (!driver.findElement(permanentpincode).getAttribute("value")
				.matches(Concierge.ConciergeData.permanentpincodeText)) {
			Log.info("permanentarea is not passed");
			return false;
		}

		if (!driver.findElement(nativeplace).getAttribute("value")
				.matches(Concierge.ConciergeData.nativeplaceText)) {
			Log.info("nativeplace is not passed");
			return false;
		}

		if (!driver.findElement(educationalqualification).getAttribute("value")
				.matches(Concierge.ConciergeData.educationalqualificationText)) {
			Log.info("educationalqualification is not passed");
			return false;
		}

		if (!driver.findElement(language).getAttribute("value")
				.matches(Concierge.ConciergeData.language1Text)) {
			Log.info("language is not passed");
			return false;
		}

		if (!driver.findElement(language1).getAttribute("value")
				.matches(Concierge.ConciergeData.language2Text)) {
			Log.info("language1 is not passed");
			return false;
		}

		/*
		 * if (driver.findElement(imageupdate).getAttribute("src").matches(""))
		 * { Log.info("imageupdate is not passed"); return false; }
		 * 
		 * if (driver.findElement(docimage).getAttribute("src").matches("")) {
		 * Log.info("docimage  is not passed"); return false; }
		 */

		return true;

	}

}
