package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalDoctorDetailsNPhotos {

	protected WebDriver driver;
	private By linkdoctordetails = By.linkText("(E) Doctor Details");
	private By doctorpageheading = By.xpath(".//*[@id='content']/h1");
	private By searchdoctor = By.id("Hospital_doctor_autosuggest"); // monika
																	// thappar
	private By searchdoctorauto = By
			.linkText("Dr. Monika Thappar | NULL | NULL");
	/*
	 * private By opdtimefrm = By.id("DoctorOpdTiming_from_0"); private By
	 * opdtimeto = By.id("DoctorOpdTiming_to_0");
	 */
	private By addDoctor = By.xpath(".//*[@id='add_doctor_btn']");
	private By name = By.id("HospitalDoctor_name");
	private By age = By.id("HospitalDoctor_age");
	private By email = By.id("HospitalDoctor_email");
	private By mobile = By.id("HospitalDoctor_mobile");
	private By licenceno = By.id("HospitalDoctor_license_number");
	private By image = By.id("doctor_image_file_name");
	private By hospital = By.id("HospitalDoctor_0_hospital");
	private By hospitallink = By.linkText("Aakash Hospital");
	private By opdfrmdate = By.id("DoctorOpdTiming_from_0");
	private By opdtodate = By.id("DoctorOpdTiming_to_0");
	private By frmdate = By.xpath(".//*[@id='HospitalDoctor_0_from']");
	private By todate = By.xpath(".//*[@id='HospitalDoctor_0_to']");
	private By addhospital = By
			.xpath(".//*[@id='emp_history_row_0']/td[5]/input");
	private By designation = By.id("DoctorDesignation_designation");
	private String chkspeialization = "HospitalDoctor_specialization_"; // 24
	private By credential = By.id("HospitalDoctor_credential_0");
	private By researchpaper = By.id("HospitalDoctor_research_0");
	private By drpaccreditation = By.id("HospitalDoctor_accreditation_0");
	private By txtaccerditaion = By.id("HospitalDoctor_institute_0");
	private By btnsubmitbutton = By.id("submit_doctor_other");
	private By btnsubmitadddoctor = By.id("submit-hospital-doctor-form");

	private By cancelbutton = By.id("cancel_doctor_save");
	private By verifydoctorname = By
			.xpath(".//*[@id='doctor_form']/table/tbody/tr[2]/td[1]");

	private By linkedit = By
			.xpath(".//*[@id='doctor_form']/table/tbody/tr[2]/td[4]/a");

	// Photo

	private By photopageheading = By.xpath(".//*[@id='content']/h1");
	private By hospitalsectionid = By.id("Hospital_section_id");
	private By btnphoto = By.id("FileData");

	public AdminAddHospitalDoctorDetailsNPhotos(WebDriver driver) {
		this.driver = driver;
	}

	public void verifyHospitalDoctorDetailsNPhotosPage()
			{

		
			Assert.assertTrue(FrameWork
				.verifySuccessMessage(driver, doctorpageheading,
						Hospital.doctordetails.doctorpageheadingText));

	}

	public void openHospitalDoctorDetailsNPhotosPage() {

		FrameWork.Click(driver, linkdoctordetails);
	}

	public void doAddDoctorDeatils() {
		Log.info("Adding Doctor Details");
		FrameWork.Type(driver, searchdoctor,
				Hospital.doctordetails.searchdoctorText);
		FrameWork.Click(driver, searchdoctorauto);
		FrameWork.GetElement(driver, designation).clear();
		FrameWork.Type(driver, designation,
				Hospital.doctordetails.designationText);
		FrameWork.SelectByVisibleText(driver, opdfrmdate,
				Hospital.doctordetails.opdfrmdateText);
		FrameWork.SelectByVisibleText(driver, opdtodate,
				Hospital.doctordetails.opdtodateText);
		FrameWork.Click(driver, btnsubmitbutton);

		Assert.assertTrue(FrameWork.verifySuccessMessage(driver,
				verifydoctorname, Hospital.doctordetails.verifydoctorText));
		

	}

	public void openHospitalDoctorPage() {

		FrameWork.Click(driver, addDoctor);
	}

	public void doAddDoctor() throws InterruptedException {

		Log.info("Adding new doctor");
		Thread.sleep(3000);
		FrameWork.Type(driver, name, Hospital.doctordetails.nameText);
		FrameWork.Type(driver, age, Hospital.doctordetails.ageText);
		FrameWork.Type(driver, email, Hospital.doctordetails.emailText);
		FrameWork.Type(driver, mobile, Hospital.doctordetails.mobileText);
		FrameWork.Type(driver, licenceno, Hospital.doctordetails.licencenoText);
		FrameWork.Type(driver, email, Hospital.doctordetails.emailText);
		FrameWork.Type(driver, hospital, Hospital.doctordetails.hospitalText);
		FrameWork.Click(driver, hospitallink);
		FrameWork.Type(driver, frmdate, Hospital.doctordetails.frmdateText);
		FrameWork.Type(driver, todate, Hospital.doctordetails.todateText);
		FrameWork.Click(driver, addhospital);
		FrameWork.clickOnGroupInputEle(driver, chkspeialization, 0, 24);
		FrameWork.Type(driver, credential,
				Hospital.doctordetails.credentialText);
		FrameWork.Type(driver, researchpaper,
				Hospital.doctordetails.researchpaperText);
		FrameWork.SelectByVisibleText(driver, drpaccreditation,
				Hospital.doctordetails.drpaccreditationText);
		FrameWork.Type(driver, txtaccerditaion,
				Hospital.doctordetails.drpaccreditationText);
		FrameWork.Click(driver, btnsubmitadddoctor);

	}

}
