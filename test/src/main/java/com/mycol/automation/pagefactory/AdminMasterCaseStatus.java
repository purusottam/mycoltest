package com.mycol.automation.pagefactory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.AdminMasterCaseStatusData;

public class AdminMasterCaseStatus {
	
	protected WebDriver driver;
	
	private By master = By.linkText("Masters");
	private By casestatusmanagerlink = By.linkText("Cases Status Manager");
	private By addCaseStatusbutton = By.linkText("Add Cases Status");
	private By addCaseStatuspageheading = By.xpath(".//*[@id='content']/h1");
	private By emailTemplate = By.id("CasesStatus_emailTemplate");
	private By messageTemplate = By.id("CasesStatus_messageTemplate");
	private By enable = By.id("CasesStatus_enable");
	private By status = By.id("CasesStatus_status");
	private By order = By.id("CasesStatus_order");
	private By isEmailSent = By.id("CasesStatus_isEmailSent");
	private By isEnd = By.id("CasesStatus_is_end");
	private By successnotification = By.xpath(".//*[@id='content']/ul/li/div");
	private By save = By.name("yt0");
	protected String update_url = "http://admin.staging.colhealth.org/index.php/casesStatus/update/17";
	private By statusBlankfieldErrormessage = By.xpath(".//*[@id='cases-status-form']/div[1]/div");
	private By orderBlankfieldErrormessage = By.xpath(".//*[@id='cases-status-form']/div[2]/div");
	
	
	
	public AdminMasterCaseStatus(WebDriver driver) {
		this.driver = driver;
		Log.startTestCase("Starting AdminMasterCaseStatusManager ....");
	}
	
	public Boolean doVerifyCaseStatusPage() {

		Log.startTestCase("doVerifyCaseStatusPage");
		if (driver.getTitle().matches(AdminMasterCaseStatusData.title)) {
			Log.info("Pass doVerifyCaseStatusPage");
			return true;
		} else {
			Log.error("Error in doVerifyCaseStatusPage");
			return false;
		}
	}
	
	public void doOpenManageCaseStatusPage() {
		Log.info("Opening doOpenManageCaseStatusPage");
		FrameWork.Click(driver, master);
		FrameWork.Click(driver, casestatusmanagerlink);
	}
	
	public void doOpenAddCaseStatusPage() {
		Log.info("Opening doOpenAddCaseStatusPage");
		FrameWork.Click(driver, addCaseStatusbutton);
	}
	
	public Boolean doVerifyAddCaseStatusPage() {
		if (driver.getTitle().matches(AdminMasterCaseStatusData.addCaseStatustitle)
				&& FrameWork.GetText(driver, addCaseStatuspageheading).matches(
						AdminMasterCaseStatusData.addCaseStatuspageheadingText)) {
			Log.info("Pass doVerifyAddCaseStatusPage");
			return true;
		} else {
			Log.error("Error in doVerifyAddCaseStatusPage");
			return false;
		}
	}
	
	public void doAddCaseStatus() throws InterruptedException {
		Log.info("Adding doAddCaseStatus");
		
		FrameWork.Type(driver, status, AdminMasterCaseStatusData.statusText);
		
		FrameWork.Type(driver, order, AdminMasterCaseStatusData.orderText);
		
		FrameWork.SelectCheckBox(driver, isEmailSent);
		
		FrameWork.SelectCheckBox(driver, isEnd);
		
		FrameWork.SelectByVisibleText(driver, emailTemplate, AdminMasterCaseStatusData.emailTemplateText);
		
		FrameWork.SelectByVisibleText(driver, messageTemplate, AdminMasterCaseStatusData.messageTemplateText);
		
		FrameWork.SelectByVisibleText(driver, enable, AdminMasterCaseStatusData.enableText);
		
        FrameWork.Click(driver, save);
		Thread.sleep(3000);
		String successNotification = "Case status "
				+ AdminMasterCaseStatusData.statusText + " added Successfully";
		FrameWork.verifySuccessMessage(driver, successnotification,	successNotification);
	}
	
	public void doUpdateCaseStatus() throws InterruptedException {
		driver.get(update_url);
		doAddCaseStatus();
		driver.get(update_url);
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, status,
					AdminMasterCaseStatusData.statusText));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, order,
					AdminMasterCaseStatusData.orderText));
	}
	
	public void doAddCaseStatusBlankField() throws InterruptedException {
		doOpenManageCaseStatusPage();
		doOpenAddCaseStatusPage();
		
		driver.findElement(status).clear();
		driver.findElement(order).clear();
		FrameWork.Click(driver, save);
		
		if(FrameWork.verifyErrorMessage(driver, statusBlankfieldErrormessage,
					AdminMasterCaseStatusData.statusBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, orderBlankfieldErrormessage,
					AdminMasterCaseStatusData.orderBlankfieldErrormessageText)){
			Log.info("Pass doAddCaseStatusBlankField");
			} else {
				Log.error("Error in doAddCaseStatusBlankField");
		}
	}

}
