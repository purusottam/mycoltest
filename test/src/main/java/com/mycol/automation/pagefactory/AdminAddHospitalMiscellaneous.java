package com.mycol.automation.pagefactory;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.Hospital;

public class AdminAddHospitalMiscellaneous {
	protected WebDriver driver;
	
	By linkpage = By.xpath(".//*[@id='leftSubLinks1']/li[8]/a");
	By pageheading = By.xpath(".//*[@id='content']/h1");
	By disfrmairport = By.id("Hospital_distance_from_airport");
	By twowheelparkingslot = By.id("Hospital_two_wheeler_parking_slots");
	By fourwheelparkingslot = By.id("Hospital_four_wheeler_parking_slots");
	By twowheelparkingcharge = By.id("Hospital_two_wheeler_parking_charge");
	By fourwheelparkcharge = By.id("Hospital_four_wheeler_parking_charge");
	
	By drpmetrostationfrm = By.id("Hospital_station_name_0");
	By metrostationfrmcount = By.id("Hospital_metro_station_0");
	By railwaystationfrm  = By.id("Hospital_railway_station_name_0");
	By railwaystationfrmcount = By.id("Hospital_railway_station_distance_0");
	By distancefrmbusdiponame = By.id("Hospital_bus_depot_name_0");
	By distancefrmbusdipokm = By.id("Hospital_bus_depot_distance_0");
	By btnsubmit = By.xpath(".//*[@id='hospital-form-8']/div[9]/input");
	By successnotification = By.id("msg-hospital-form-8");
	
		
	public AdminAddHospitalMiscellaneous(WebDriver driver) {
				this.driver = driver;
	}
	
	public void openAddHospitalMiscellaneousPage() {
		
		FrameWork.Click(driver, linkpage);
	}
	
	public void verifyAddHospitalMiscellaneousPage() throws InterruptedException
	{
		
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, pageheading, Hospital.miscellaneous.pageheadingText),"Heading is not matched");
	}
	
	public void doAddHospitalMiscellaneous()
	{
		Log.startTestCase("doAddHospitalMiscellaneous");
		FrameWork.Type(driver, disfrmairport,Hospital.miscellaneous.disfrmairport);
		FrameWork.Type(driver, twowheelparkingslot,Hospital.miscellaneous.twowheelparkingslot);
		FrameWork.Type(driver, fourwheelparkingslot,Hospital.miscellaneous.fourwheelparkingslot);
		FrameWork.Type(driver, fourwheelparkcharge,Hospital.miscellaneous.fourwheelparkcharge);
		FrameWork.Type(driver, twowheelparkingcharge, Hospital.miscellaneous.twowheelparkingcharge);
		FrameWork.SelectByVisibleText(driver, drpmetrostationfrm, Hospital.miscellaneous.drpmetrostationfrm);
		FrameWork.Type(driver, metrostationfrmcount, Hospital.miscellaneous.metrostationfrmcount);
		FrameWork.Type(driver, railwaystationfrm, Hospital.miscellaneous.railwaystationfrm);
		FrameWork.Type(driver, railwaystationfrmcount, Hospital.miscellaneous.railwaystationfrmcount);
		FrameWork.Type(driver, distancefrmbusdiponame, Hospital.miscellaneous.distancefrmbusdiponame);
		FrameWork.Type(driver, distancefrmbusdipokm, Hospital.miscellaneous.distancefrmbusdipokm);
		FrameWork.Click(driver,btnsubmit);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, successnotification, Hospital.miscellaneous.successnotification));
		Log.endTestCase("doAddHospitalMiscellaneous");
	}
	
	
	public void doVerifyHospitalMiscellaneous()
	{
		Log.startTestCase("doVerifyHospitalMiscellaneous");
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, disfrmairport,Hospital.miscellaneous.disfrmairport));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, twowheelparkingslot,Hospital.miscellaneous.twowheelparkingslot));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, fourwheelparkingslot,Hospital.miscellaneous.fourwheelparkingslot));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, fourwheelparkcharge,Hospital.miscellaneous.fourwheelparkcharge));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, twowheelparkingcharge, Hospital.miscellaneous.twowheelparkingcharge));
		Assert.assertTrue(FrameWork.verifyDropDownData(driver, drpmetrostationfrm, Hospital.miscellaneous.drpmetrostationfrm));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, metrostationfrmcount, Hospital.miscellaneous.metrostationfrmcount));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, railwaystationfrm, Hospital.miscellaneous.railwaystationfrm));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, railwaystationfrmcount, Hospital.miscellaneous.railwaystationfrmcount));
		Assert.assertTrue(FrameWork.verifInputFieldData(driver, distancefrmbusdiponame, Hospital.miscellaneous.distancefrmbusdiponame));
		Log.endTestCase("doVerifyHospitalMiscellaneous");
	}
}
