package com.mycol.automation.pagefactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.auriga.automation.common.FrameWork;
import com.auriga.automation.common.Log;
import com.mycol.automation.factorydata.MyReminderData;

public class MyReminder {
	protected WebDriver driver;

	public MyReminder(WebDriver driver) {
		this.driver = driver;
	}
	
	private By welcome = By.id("user_links");
	private By myReminder = By.linkText("My Reminder");
	private By addNewReminder = By.linkText("Add New Reminder");
	private By txtTitle = By.name("defaultOptions[title]");
	private By txtDrugName = By.id("drugName-1");
//	private By txtPillShape = By.xpath(".//*[@id='tabs-1']/div[2]/div[2]/div/div/div/div/label[3]");
	private By txtTime = By.name("data[0][time][0][epochTime]");
	public String txtTime1 = "";
	public String txtTime2 = "";
	public String txtTime3 = "";
	public String txtTime4 = "";
	public String txtQuantity1 = "";
	public String txtQuantity2 = "";
	public String txtQuantity3 = "";
	public String txtQuantity4 = "";
	private By txtStartDate = By.id("startdate-value");
//	private By btnDuration_NoDays = By.xpath(".//*[@id='duration']/div/label[2]");
//	private By txtDaysNumber = By.id("days_number-1");
//	private By btnDays = By.xpath(".//*[@id='days']/div[1]/label[1]");
	private By drpdwnAlarm = By.name("defaultOptions[alarm]");
	private By btnCareTaker = By.xpath("/html/body/div[1]/div[2]/fieldset/form/div[7]/div/div/label[2]");
	private By btnSave = By.xpath(".//*[@id='reminder_form']/div[8]/div/button");
	private By btnAddDrug = By.id("addDrug");
	private By appointmentReminderType = By.xpath(".//*[@id='reminder_form']/div[2]/div/label[2]");
	private By appointmentTitle = By.name("defaultOptions[title]");
	private By appointmentDate = By.id("ddate-value");
	private By appointmentTime = By.name("appointmentData[time][0][epochTime]");
	private By appointmentHospitalName = By.name("appointmentData[hospital]");
	private By appointmentDoctorName = By.name("appointmentData[doctor]");
	private By appointmentAlarm = By.name("defaultOptions[alarm]");
	private By appointmentCaretaker = By.xpath(".//*[@id='cateTakerCont']/label[2]");
	private By appointmentSave = By.xpath(".//*[@id='reminder_form']/div[8]/div/button");
	private By titleBlankfieldErrormessage = By.id("defaultOptions[title]-error");
	private By drugNameBlankfieldErrormessage = By.id("drugName-1-error");
	private By alarmBlankfieldErrormessage = By.id("defaultOptions[alarm]-error");
	private By caretakerBlankfieldErrormessage = By.id("defaultOptions[caretaker]-error");
	private By dateBlankfieldErrormessage = By.id("ddate-value-error");
	private By timeBlankfieldErrormessage = By.id("appointmentData[time][0][epochTime]-error");
	private By hospitalNameBlankfieldErrormessage = By.id("hospital-error");
	private By doctorNameBlankfieldErrormessage = By.id("doctor-error");
	private By reminderSuccessMsg = By.xpath(".//*[@id='content']/div[1]");
	private By autoSuggest1 = By.xpath(".//*[@id='reminder_form']/div[5]/div/div[3]/div/span/span/div/span/div[1]/p");
	private By autoSuggest2 = By.xpath(".//*[@id='reminder_form']/div[5]/div/div[4]/div/span/span/div/span/div[1]/p");
	public String specificDaysOfWeek = "";
	public String daysInterval = "";
	private By txtDaysInterval = By.id("days_interval-1");
	private By Monday = By.xpath(".//*[@id='weekDays-1']/label[1]/input");
	private By Tuesday = By.xpath(".//*[@id='weekDays-1']/label[2]/input");
	private By Wednesday = By.xpath(".//*[@id='weekDays-1']/label[3]/input");
	private By Thursday = By.xpath(".//*[@id='weekDays-1']/label[4]/input");
	private By Friday = By.xpath(".//*[@id='weekDays-1']/label[5]/input");
	public String remindrTime ="";
	
	
	public void doOpenMyReminderPage() {
		FrameWork.Click(driver, welcome);
		FrameWork.Click(driver, myReminder);
		Log.info("opening My Reminders page");
	}
	
	public void doVerifyMyReminderPage() {
		Assert.assertEquals(driver.getTitle(), MyReminderData.title);
		
	}
	
	public void doAddMyReminderPage()
	{
		
		FrameWork.Click(driver, addNewReminder);
		FrameWork.Type(driver, txtTitle, MyReminderData.txtTitle);
		
	}
	
	public void doAddReminderWithLoop(int count,String daySchedule, String timeReminder) throws InterruptedException
	{
		for(int i=0;i<=count;i++)
		{
			if(i>=1){
		FrameWork.Click(driver, btnAddDrug);
			}
		//String drugName = "drugName-"+i+1;
		
		FrameWork.Type(driver, By.id("drugName-"+(i+1)), MyReminderData.txtDrugName);
		FrameWork.Click(driver, By.xpath(".//*[@id='tabs-"+(i+1)+"']/div[2]/div[2]/div/div/div/div/label[3]"));
		//FrameWork.SelectByVisibleText(driver, By.name("data["+i+"][mTimes][value]"), MyReminderData.drpdwnReminderTimesText2);
		remindrTime = "data["+i+"][mTimes][value]";
		txtTime1 = "data["+i+"][time][0][epochTime]";
		txtTime2 = "data["+i+"][time][1][epochTime]";
		txtTime3 = "data["+i+"][time][2][epochTime]";
		txtTime4 = "data["+i+"][time][3][epochTime]";
		txtQuantity1 = "data["+i+"][quantity][0]";
		txtQuantity2 = "data["+i+"][quantity][1]";
		txtQuantity3 = "data["+i+"][quantity][2]";
		txtQuantity4 = "data["+i+"][quantity][3]";

		doDifferentReminderTimes(timeReminder);
/*		FrameWork.SelectByVisibleText(driver, By.name("data["+i+"][mTimes][value]"), MyReminderData.drpdwnReminderTimesText);
		FrameWork.Type(driver, By.name("data["+i+"][time][0][epochTime]"), MyReminderData.txtTime1);
		FrameWork.Type(driver, By.name("data["+i+"][time][1][epochTime]"), MyReminderData.txtTime2);
		FrameWork.Type(driver, By.name("data["+i+"][quantity][0]"), MyReminderData.txtQuantity1);
		FrameWork.Type(driver, By.name("data["+i+"][quantity][1]"), MyReminderData.txtQuantity2);*/
		FrameWork.Type(driver, By.name("data["+i+"][save_date]"), MyReminderData.txtStartDate);
		FrameWork.Click(driver, By.xpath("html/body/div[1]/div[2]/fieldset/form/div[4]/div/div[2]/div["+(i+1)+"]/div[4]/div[2]/div/div/div[2]/div/div/label[2]"));
		//FrameWork.clickOnListElementByName(driver, btnDuration, 1);
        
		FrameWork.Type(driver, By.id("days_number-"+(i+1)), MyReminderData.txtDaysNumber);
		
		daysInterval = "html/body/div[1]/div[2]/fieldset/form/div[4]/div/div[2]/div["+(i+1)+"]/div[4]/div[2]/div/div/div[3]/div[1]/div/label[3]";
		specificDaysOfWeek = "html/body/div[1]/div[2]/fieldset/form/div[4]/div/div[2]/div["+(i+1)+"]/div[4]/div[2]/div/div/div[3]/div[1]/div/label[2]";
		
		doDifferentDaysSchedule(daySchedule);
		//FrameWork.clickOnListElementByName(driver, btnDays, 0);
		
		FrameWork.SelectByVisibleText(driver, drpdwnAlarm, MyReminderData.drpdwnAlarmText);
		//FrameWork.clickOnListElementByName(driver, btnCareTaker, 0);
		FrameWork.Click(driver, btnCareTaker);
		
		}
		
		FrameWork.Click(driver, btnSave);
		//Thread.sleep(2000);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, reminderSuccessMsg,
											MyReminderData.reminderSuccessMsgText));	
	}
	
	public void doAddMyAppointmentPage() throws InterruptedException
	{
		doOpenMyReminderPage();
		FrameWork.Click(driver, addNewReminder);
		FrameWork.Click(driver, appointmentReminderType);
		FrameWork.Type(driver, appointmentTitle, MyReminderData.appointmentTitleText);
		FrameWork.Type(driver, appointmentDate, MyReminderData.appointmentDateText);
		FrameWork.Type(driver, appointmentTime, MyReminderData.appointmentTimeText);
		FrameWork.Click(driver, appointmentCaretaker);
		FrameWork.Type(driver, appointmentHospitalName, MyReminderData.appointmentHospitalNameText);
		FrameWork.Click(driver, autoSuggest1);
		FrameWork.Type(driver, appointmentDoctorName, MyReminderData.appointmentDoctorNameText);
		FrameWork.Click(driver, autoSuggest2);
		FrameWork.SelectByVisibleText(driver, appointmentAlarm, MyReminderData.appointmentAlarmText);
		FrameWork.Click(driver, btnCareTaker);
		FrameWork.Click(driver, appointmentSave);
		//Thread.sleep(2000);
		Assert.assertTrue(FrameWork.verifySuccessMessage(driver, reminderSuccessMsg,
											MyReminderData.reminderSuccessMsgText));
		
	}
	
	public void AddReminderBlankFields(){
		boolean flag;
		
		doOpenMyReminderPage();
		
		FrameWork.Click(driver, addNewReminder);
		
		driver.findElement(txtTitle).clear();
		driver.findElement(txtDrugName).clear();
		driver.findElement(txtTime).clear();
		driver.findElement(txtStartDate).clear();
		FrameWork.SelectByVisibleText(driver, drpdwnAlarm, MyReminderData.drpdwnAlarmDefaultText);
		driver.findElement(btnSave).click();
		
		if(FrameWork.verifyErrorMessage(driver, titleBlankfieldErrormessage,
					MyReminderData.titleBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, drugNameBlankfieldErrormessage,
					MyReminderData.drugNameBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, alarmBlankfieldErrormessage,
					MyReminderData.alarmBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, caretakerBlankfieldErrormessage,
					MyReminderData.caretakerBlankfieldErrormessageText)){
			flag=true;
			Log.info("Pass AddReminderBlankFields");
			} else {
				flag=false;
				Log.error("Error in AddReminderBlankFields");
			}
		Assert.assertTrue(flag);
	}
	
	public void AddAppointmentBlankFields(){
		doOpenMyReminderPage();
		
		FrameWork.Click(driver, addNewReminder);
		FrameWork.Click(driver, appointmentReminderType);
		driver.findElement(appointmentTitle).clear();
		driver.findElement(appointmentDate).clear();
		driver.findElement(appointmentTime).clear();
		driver.findElement(appointmentHospitalName).clear();
		driver.findElement(appointmentDoctorName).clear();
		FrameWork.SelectByVisibleText(driver, appointmentAlarm, MyReminderData.drpdwnAlarmDefaultText);
		driver.findElement(btnSave).click();
		
		if(FrameWork.verifyErrorMessage(driver, titleBlankfieldErrormessage,
					MyReminderData.titleBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, dateBlankfieldErrormessage,
					MyReminderData.dateBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, timeBlankfieldErrormessage,
					MyReminderData.timeBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, hospitalNameBlankfieldErrormessage,
					MyReminderData.hospitalNameBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, doctorNameBlankfieldErrormessage,
					MyReminderData.doctorNameBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, alarmBlankfieldErrormessage,
					MyReminderData.alarmBlankfieldErrormessageText)
			&& FrameWork.verifyErrorMessage(driver, caretakerBlankfieldErrormessage,
					MyReminderData.caretakerBlankfieldErrormessageText)){
			Log.info("Pass AddAppointmentBlankFields");
			} else {
				Log.error("Error in AddAppointmentBlankFields");
			}
	}
	
	public void doDifferentDaysSchedule(String daySchedule){
		Log.info("Now i am in Schedule");
		if(daySchedule.equals("Specific days of week")){
			By sDOW = By.xpath(specificDaysOfWeek);
			FrameWork.Click(driver, sDOW);
			FrameWork.SelectCheckBox(driver, Monday);
			FrameWork.SelectCheckBox(driver, Tuesday);
			FrameWork.SelectCheckBox(driver, Wednesday);
			FrameWork.SelectCheckBox(driver, Thursday);
			FrameWork.SelectCheckBox(driver, Friday);
		}
		else if(daySchedule.equals("Days Interval")){
			By dI = By.xpath(daysInterval);
			FrameWork.Click(driver, dI);
			FrameWork.Type(driver, txtDaysInterval, "1");
		}
		
	}
	
	public void doDifferentReminderTimes(String timeReminder){
		Log.info("Now i am in time reminder");
		if(timeReminder == "Once a day"){
			By e = By.name(remindrTime);
			By T1 = By.name(txtTime1);
			By Q1 = By.name(txtQuantity1);
			FrameWork.SelectByVisibleText(driver, e, MyReminderData.drpdwnReminderTimesText1);
			FrameWork.Type(driver, T1, MyReminderData.txtTime1);
			FrameWork.SelectByVisibleText(driver, Q1, MyReminderData.txtQuantity1);
		}
		if(timeReminder == "Twice a day"){
			By e = By.name(remindrTime);
			By T1 = By.name(txtTime1);
			By T2 = By.name(txtTime2);
			By Q1 = By.name(txtQuantity1);
			By Q2 = By.name(txtQuantity2);
			FrameWork.SelectByVisibleText(driver, e, MyReminderData.drpdwnReminderTimesText2);
			FrameWork.Type(driver, T1, MyReminderData.txtTime1);
			FrameWork.SelectByVisibleText(driver, Q1, MyReminderData.txtQuantity1);
			FrameWork.Type(driver, T2, MyReminderData.txtTime2);
			FrameWork.SelectByVisibleText(driver, Q2, MyReminderData.txtQuantity2);
		}
		if(timeReminder == "3 Times a day"){
			By e = By.name(remindrTime);
			By T1 = By.name(txtTime1);
			By T2 = By.name(txtTime2);
			By T3 = By.name(txtTime3);
			By Q1 = By.name(txtQuantity1);
			By Q2 = By.name(txtQuantity2);
			By Q3 = By.name(txtQuantity3);
			FrameWork.SelectByVisibleText(driver, e, MyReminderData.drpdwnReminderTimesText3);
			FrameWork.Type(driver, T1, MyReminderData.txtTime1);
			FrameWork.SelectByVisibleText(driver, Q1, MyReminderData.txtQuantity1);
			FrameWork.Type(driver, T2, MyReminderData.txtTime2);
			FrameWork.SelectByVisibleText(driver, Q2, MyReminderData.txtQuantity2);
			FrameWork.Type(driver, T3, MyReminderData.txtTime3);
			FrameWork.SelectByVisibleText(driver, Q3, MyReminderData.txtQuantity3);
		}
		if(timeReminder == "4 Times a day"){
			By e = By.name(remindrTime);
			By T1 = By.name(txtTime1);
			By T2 = By.name(txtTime2);
			By T3 = By.name(txtTime3);
			By T4 = By.name(txtTime4);
			By Q1 = By.name(txtQuantity1);
			By Q2 = By.name(txtQuantity2);
			By Q3 = By.name(txtQuantity3);
			By Q4 = By.name(txtQuantity4);
			FrameWork.SelectByVisibleText(driver, e, MyReminderData.drpdwnReminderTimesText4);
			FrameWork.Type(driver, T1, MyReminderData.txtTime1);
			FrameWork.SelectByVisibleText(driver, Q1, MyReminderData.txtQuantity1);
			FrameWork.Type(driver, T2, MyReminderData.txtTime2);
			FrameWork.SelectByVisibleText(driver, Q2, MyReminderData.txtQuantity2);
			FrameWork.Type(driver, T3, MyReminderData.txtTime3);
			FrameWork.SelectByVisibleText(driver, Q3, MyReminderData.txtQuantity3);
			FrameWork.Type(driver, T4, MyReminderData.txtTime4);
			FrameWork.SelectByVisibleText(driver, Q4, MyReminderData.txtQuantity4);
		}
		
	}
	
}