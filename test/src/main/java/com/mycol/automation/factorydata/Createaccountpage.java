package com.mycol.automation.factorydata;

public class Createaccountpage {
	
	public static class CreateaccountpageData
	{
		public static String success = "Register Successfully. Please verify Your Email.";
		public static String name = "purusottam334@mailinator.com";
		public static String existing_email= "purusottam.k@aurigait.com";
		public static String mobile = "1111111111";
		public static String existing_mobile = "9782292499";
		public static String popupcity = "KMR";
		public static String password = "12345";
		public static String repassword = "12345";
		public static String nameError = "Please enter Mobile / Email.";
		public static String nameErrorForVaild = "Please enter a valid Email or Mobile No";
		public static String passwordError = "Please provide a password";
		public static String repasswordError = "Please provide a password";
		public static String PageHeading = "Please sign up";
		public static String nameplaceholder = "Mobile No / Email address";
		public static String passwordplaceholder = "Password";
		public static String repasswordplaceholder = "Confirm Password";
		public static String mobilesuccess = "Register Successfully. Please verify OPT send on your mobile.";
		public static String existingmobileText = "mobile already exist";
		public static String existing_emailText= "email already exist";
		
	}

}
