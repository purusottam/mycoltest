package com.mycol.automation.factorydata;

public class Invitefamilymemberspage {
	
	public static class InvitefamilymemberspageData
	{
		public static String mobiletext = "1122338555";
		public static String firstnametext = "ramji";
		public static String lastnametext = "lal";
		public static String relation = "Brother";
		
		
		public static String pageheadingtext = "Invite Family Members";
		public static String successmessage ="Relationship Added.";
	}

}
