package com.mycol.automation.factorydata;

public class AdminmastercitygroupsPage {
	
	public static class AdminmastercitygroupsData
	{
		public static String title = "My Col - Admin CityGroup";
		public static String addtitle = "My Col - Create CityGroup";
		public static String nameText = "Maharashtra";
		public static String statusText = "Enable";
		public static String centr_latitudeText = "18.9600° N";
		public static String centr_longitudeText = "72.8200° E";
		public static String addpageheadingText = "Create CityGroup";
		public static String updatetitle =  "My Col - Update CityGroup";
		public static String allblankfielderrormessageText = "Name cannot be blank.";
		
	}

}
