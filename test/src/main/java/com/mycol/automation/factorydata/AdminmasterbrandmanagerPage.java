package com.mycol.automation.factorydata;

public class AdminmasterbrandmanagerPage {
	
	public static class AdminmasterbrandmanagerData
	{
		public static String title = "My Col - Admin Brands";
		public static String addbrandtitle = "My Col - Create Brands";
		public static String brand_nameText = "myCOL";
		public static String statusText = "Enable";
		public static String addbrandpageheadingText = "Create Brand";
		//public static String updatetitle =  "My Col - Update CityGroup";
		public static String brandBlankfieldErrormessageText = "Brand cannot be blank.";
	
	}
}