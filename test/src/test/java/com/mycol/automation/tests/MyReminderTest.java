package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.MyReminder;
import com.mycol.automation.pagefactory.SignInPage;

public class MyReminderTest extends TestBaseSetup {
	private WebDriver driver;

	private SignInPage signInPage;
	private MyReminder myreminder;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		myreminder = new MyReminder(driver);
		signInPage = new SignInPage(driver);

	}

	@Test(priority = 0, enabled = true)
	public void doOpenMyReminderPageTest() throws InterruptedException {
		signInPage.doLogin();
		myreminder.doOpenMyReminderPage();
	}

	@Test(priority = 1, enabled = true)
	public void doVerifyPageHeadingTest() {
		myreminder.doVerifyMyReminderPage();
	}
	
	@Test(priority = 2, enabled = true)
	public void AddReminderBlankFieldsTest() {
		myreminder.AddReminderBlankFields();
	}
	
	@Test(priority = 3, enabled = true)
	public void AddAppointmentBlankFieldsTest() {
		myreminder.AddAppointmentBlankFields();
	}

	@Test(priority = 4, enabled = true)
	public void doAddMyReminderTest() throws InterruptedException {
		myreminder.doOpenMyReminderPage();
		myreminder.doAddMyReminderPage();
		myreminder.doAddReminderWithLoop(2, "Days Interval", "Twice a day");
		//We can use different combinations of Day Schedules(Specific days of week,
		//Days Interval) and Time Reminders (Once a day, Twice a day, 
		//3 Times a day, 4 Times a day)
	}
	
	@Test(priority = 5, enabled = true)
	public void doAddMyAppointmentTest() throws InterruptedException {
		myreminder.doAddMyAppointmentPage();
	}

}
