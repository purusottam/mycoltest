package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.Log;
import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.*;

public class AdminAddHospitalTest extends TestBaseSetup {
	private WebDriver driver;
	AdminLogin adminlogin;
	AdminAddHospitalGenral genral;
	AdminAddHospitalPatientDetails patient;
	AdminAddHospitalDepartmentnFacility depatment;
	AdminAddHospitalDoctorDetailsNPhotos doctor;
	AdminAddHospitalPayments payment;
	AdminAddHospitalMiscellaneous miscellaneous;
	AdminAddHospitalMap map ;
	AdminAddHospitalAccommodation accomdation;
	AdminAddHospitalRoomDetails roomdetail;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
		adminlogin = new AdminLogin(driver);
		genral = new AdminAddHospitalGenral(driver);
		roomdetail = new AdminAddHospitalRoomDetails(driver);
		patient = new AdminAddHospitalPatientDetails(driver);
		depatment = new AdminAddHospitalDepartmentnFacility(driver);
		doctor = new AdminAddHospitalDoctorDetailsNPhotos(driver);
		payment = new AdminAddHospitalPayments(driver);
		miscellaneous = new AdminAddHospitalMiscellaneous(driver);
		map = new AdminAddHospitalMap(driver);
		accomdation = new AdminAddHospitalAccommodation(driver);
		
	}
	
	// Genral

	@Test(priority=0,enabled = true)
	public void openManageHospitalTest() {
		adminlogin.doLogin();
		genral.openManageHospital();
		Assert.assertTrue(genral.doVerifyManageHospital(),"Verifying manage hospital");
	}
	@Test(priority=1,enabled = true)
	public void openAddHospitalTest()
	{
		//genral.openEditHospital();
		genral.openAddGenralHospitalPage();
	}
	@Test(priority=2,enabled = true)
	public void doAddGenralHospitalTest() {
		
		Assert.assertTrue(genral.doAddGenralHospital(), "Hospital not added");
		Log.info("in second test");

	}
	@Test(priority=3,enabled = true)
	public void verifyAddedHospitalTest() {
		genral.verifyAddedGenralHospital();
					
	}

	
	@Test(priority=4,enabled = true)
	public void blankMandatoryFieldTest()
	{
		genral.clearAll();
		Assert.assertTrue(genral.blankMandatoryField());
	}
			
	
		
	//Room Details
	@Test(priority=5,enabled = true)
	public void openRoomDetailPageTest()
	{
		roomdetail.openRoomDetailPage();
	}
	
	@Test(priority=6,enabled = true)
	public void verifyRoomDetailPageTest()
	{
		roomdetail.verifyRoomDetailPage();
	}
	
	
	@Test(priority=7,enabled = true)
	public void doAddRoomHospitalTest()
	{
		roomdetail.clearAllField();
		roomdetail.doAddRoomHospital();
	}
	
	@Test(priority=8,enabled = true)
	public void verifyAddedGenralHospitalTest()
	{
		roomdetail.verifyAddedRoomHospital();
	}
	
	 // Patient Details
	
	@Test(priority=9,enabled = true)
	public void openPatientDetailPageTest()
	{
		patient.openPatientDetailPage();
	}
	
	@Test(priority=10,enabled = false)
	public void verifyPatientDetailPage()
	{
		patient.verifyPatientDetailPage();
		//patient.clearAll();
	}
	
	@Test(priority=11,enabled = true)
	public void doAddPatientDetailHospitalTest() throws InterruptedException
	{
		patient.clearAll();
		patient.doAddPatientDetailHospital();
	}

	@Test(priority=12,enabled = true)
	public void verifyAddedPatientDetailHospitalTest()
	{
		patient.verifyAddedPatientDetailHospital();
	}
	
	
	// Departmet  and Facilities 
	
	@Test(priority=10,enabled = true)
	public void openHospitalDepartmentnFacilityPageTest()
	{
		depatment.openHospitalDepartmentnFacilityPage();
	}
	
	@Test(priority=11,enabled = false)
	public void verifyHospitalDepartmentnFacilityPageTest()
	{
		depatment.verifyHospitalDepartmentnFacilityPage();
	}
	
	@Test(priority=12,enabled = true)
	public void doHospitalDepartmentnFacilityPageTest()
	{
		depatment.doHospitalDepartmentnFacilityPage();
	}
	
	@Test(priority=13,enabled = true)
	public void verifyDepartmentnFacilityPageTest()
	{
		depatment.verifyDepartmentnFacilityPage();
	}
	

	// Doctors Details
	
	@Test(priority=14,enabled = true)
	public void openHospitalDoctorDetailsNPhotosPageTest()
	{
		doctor.openHospitalDoctorDetailsNPhotosPage();
	}
	
	@Test(priority=15,enabled = true)
	public void verifyHospitalDoctorDetailsNPhotosPageTest() throws InterruptedException
	{
		doctor.verifyHospitalDoctorDetailsNPhotosPage();
	}
	
	@Test(priority=16,enabled = true)
	public void doAddDoctorDeatilsTest()
	{
		doctor.doAddDoctorDeatils();
	}
	
	@Test(priority=17,enabled = true)
	public void doAddDoctorTest() throws InterruptedException
	{
		doctor.openHospitalDoctorPage();
		Thread.sleep(3000);
		doctor.doAddDoctor();
	}
		
	
	// Payments
	
	@Test(priority=17,enabled = true)
	public void openAddHospitalPaymentsPageTest()
	{
		payment.openAddHospitalPaymentsPage();
	}
	
	@Test(priority=18,enabled = true)
	public void verifyHospitalPaymentsPageTest()
	{
		payment.verifyHospitalPaymentsPage();
	}
	
	@Test(priority=19,enabled = true)
	public void doAddPaymentTest()
	{
		payment.doAddPayment();
	}
	
	
	// Miscellaneous 
	
	@Test(priority=20,enabled = true)
	public void openAddHospitalMiscellaneousPage()
	{
		miscellaneous.openAddHospitalMiscellaneousPage();
	}
	
	@Test(priority=21,enabled = true)
	public void verifyAddHospitalMiscellaneousPageTest() throws InterruptedException
	{
		Thread.sleep(3000);
		miscellaneous.verifyAddHospitalMiscellaneousPage();
	}
	
	@Test(priority=22,enabled = true)
	public void doAddHospitalMiscellaneousTest()
	{
		miscellaneous.doAddHospitalMiscellaneous();
	}
	
	@Test(priority=23,enabled = true)
	public void doVerifyHospitalMiscellaneousTest()
	{
		miscellaneous.doVerifyHospitalMiscellaneous();
	}
	
	
	// Google map 
	
	@Test(priority=24,enabled = true)
	public void openAddHospitalMapPageTest()
	{
		map.openAddHospitalMapPage();
	}
	
	@Test(priority=25,enabled = true)
	public void verifyAddHospitalMapPageHeading() throws InterruptedException
	{
		Thread.sleep(3000);
		map.verifyAddHospitalMapPageHeading();
	}
	
	@Test(priority=26,enabled = true)
	public void doAddHospitalMapTest()
	{
		map.doAddHospitalMap();
	}
	
	@Test(priority=27,enabled = true)
	public void verifyHospitalMapTest()
	{
		map.verifyHospitalMap();
	}
	
	
	// Accommadation
	
	@Test(priority=28,enabled = true)
	public void openAddHospitalAccommodationPageTest()
	{
		accomdation.openAddHospitalAccommodationPage();
	}
	
	@Test(priority=29,enabled = true)
	public void verifyAddHospitalAccommodationHeadingTest() throws InterruptedException
	{
		Thread.sleep(3000);
		accomdation.verifyAddHospitalAccommodationHeading();
	}
	
	@Test(priority=30,enabled = true)
	public void doAddHospitalAccommodationTest()
	{
		accomdation.doAddHospitalAccommodation();
	}
	
	@Test(priority=31,enabled = true)
	public void verifyHospitalAccommodationTest()
	{
		accomdation.verifyHospitalAccommodation();
	}
	
}
