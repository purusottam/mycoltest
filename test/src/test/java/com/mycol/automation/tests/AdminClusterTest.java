package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.AddCluster;
import com.mycol.automation.pagefactory.AdminLogin;

public class AdminClusterTest extends TestBaseSetup {

	private WebDriver driver;
	AddCluster addcluster;
	AdminLogin signin;

	@BeforeClass
	public void setup() {
		driver = getDriver();
		addcluster = new AddCluster(driver);
		signin = new AdminLogin(driver);
	}

	@Test(priority = 0, enabled = true)
	public void doOpenManageClusterPageTest() {
		signin.doLogin();
		addcluster.doOpenManageClusterPage();
	}
	
	@Test(priority = 1, enabled = true)
	public void doVerifyClusterManagePageTest() {

		Assert.assertTrue(addcluster.doVerifyClusterManagePage(),
				"Cluster manage page is not opened ");
	}

	@Test(priority = 2, enabled = true)
	public void doVerifyAddClusterPageTest() {
		addcluster.doOpenAddClusterPage();
		Assert.assertTrue(addcluster.doVerifyAddClusterPage());
	}

	@Test(priority = 3, enabled = true)
	public void doAddClusterTest() throws InterruptedException {
		addcluster.doAddCluster();
	}

	@Test(priority = 4, enabled = false)
	public void doUpdateClusterTest() {
		addcluster.doUpdateCluster();
	}

	@Test(priority = 5, enabled = true)
	public void doBlankAllFieldTest() {
		addcluster.doOpenManageClusterPage();
		addcluster.doOpenAddClusterPage();
		addcluster.doBlankAllField();
	}

	
	
}
