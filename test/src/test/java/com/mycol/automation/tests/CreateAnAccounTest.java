package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.factorydata.Createaccountpage;
import com.mycol.automation.pagefactory.*;

public class CreateAnAccounTest extends TestBaseSetup {
	private WebDriver driver;
	private SignInPage signInPage;

	private CreateAccountPage createAccountPage;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
	
		createAccountPage = new CreateAccountPage(driver);
		
	}

	@Test(priority = 0, enabled = true)
	public void verifyPageHeading() {
		System.out.println("Verifying Page Heading...");
		createAccountPage.openRegisterPage();
		Assert.assertTrue(createAccountPage.verifyPageHeading(), "Verifying page heading");

	}
	
	@Test(priority = 9, enabled = true)
	public void createWrongAccountByAlphabatFunction() {
		System.out.println("Register by Wrong Account By Only Alphabat ...");
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.createWrongAccountByAlphabat();
		}
	
@Test(priority = 10, enabled = false)
	public void createAccountByMobileFunction() {
		System.out.println("Registering...");
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.createAccountByMobile();
		Assert.assertTrue(createAccountPage.verifyMobileRegisterAccount(),"User Not Registered Successfully.");

	}
	
	@Test(priority = 1, enabled = true)
	public void createAccountByEmailFunction() throws InterruptedException {
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.createAccountByEmail();
		Thread.sleep(3000);
		createAccountPage.verifyEmailRegisterAccount();

	}
	

	@Test(priority = 2, enabled = true)
	public void blankMobileOrEmailFunction() {
	
		System.out.println("Testing blank MobileOrEmail ...");
		createAccountPage.openRegisterPage();
		createAccountPage.blankMobileOrEmail();

	}
	
	@Test(priority = 3, enabled = true)
	public void blankPasswordFunction() {
		
		System.out.println("Testing blank Password  ...");
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.blankPassword();

	}
	
	@Test(priority = 4, enabled = true)
	public void blankRePasswordFunction() {
		System.out.println("Testing RePassword blank ...");
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.blankPassword();

	}
	
	
	@Test(priority = 6,enabled = true)
	public void verifyemailormobilePlaceHolderTextFunction() {
		System.out.println("Testing emailormobile PlaceHolder Text ...");
		createAccountPage.openRegisterPage();
		Assert.assertTrue(createAccountPage.verifyemailormobilePlaceHolderText(),"After register user loged in without verification email");
		}
	
	@Test(priority = 7,enabled = true)
	public void verifyPasswordPlaceHolderTextFunction() {
		System.out.println("Testing Password PlaceHolder Text ...");
		createAccountPage.openRegisterPage();
		Assert.assertTrue(createAccountPage.verifyPasswordPlaceHolderText(),"After register user loged in without verification email");
		}
	
	@Test(priority = 8,enabled = true)
	public void verifyRePasswordPlaceHolderTextFunction() {
		System.out.println("Testing RePassword PlaceHolder Text ...");
		createAccountPage.openRegisterPage();
		Assert.assertTrue(createAccountPage.verifyRePasswordPlaceHolderText(),"After register user loged in without verification email");
		}
	@Test(priority = 9,enabled = true)
	public void createAccountByMobileTest() {
		createAccountPage.openRegisterPage();
		createAccountPage.clearRegisterField();
		createAccountPage.createAccountByMobile();
		}
	
}