package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.AdminLogin;

public class AdminLoginTest extends TestBaseSetup {

	private WebDriver driver;
	AdminLogin adminlogin;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
		adminlogin = new AdminLogin(driver);
	}

	@Test(priority = 0)
	public void clearLoginTest() {
		adminlogin.clearFields();
	}

	@Test(priority = 1)
	public void doLoginTest() {
		adminlogin.doLogin();
	}

	@Test(priority = 2)
	public void verifyLoginTest() {
		adminlogin.verifyLogin();
		adminlogin.logout();
	}

	@Test(priority = 3)
	public void blankUserNameNPasswordTest() {
		Assert.assertTrue(adminlogin.blankUserNameNPassword());
	}
}
