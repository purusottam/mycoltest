package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.AdminLogin;
import com.mycol.automation.pagefactory.AdminMasterDepartmentManager;

public class AdminMasterDepartmentManagerTest extends TestBaseSetup {
	private WebDriver driver;
	
	AdminLogin signin;
	AdminMasterDepartmentManager departmentmngr;
	
	@BeforeClass
	public void setup() {
		driver = getDriver();
		signin = new AdminLogin(driver);
		departmentmngr = new AdminMasterDepartmentManager(driver);
			}

	@Test(priority=0)
	public void doVerifyDepartmentManagerPageTest()
	{
		signin.doLogin();
		departmentmngr.doOpenDepartmentManagerPage();
		departmentmngr.doOpenAddDepartmentPage();
		Assert.assertTrue(departmentmngr.doVerifyAddDepartmentPage());
	}
	
	@Test(priority=1)
	public void doVerifyAddDepartmentPageTest() throws InterruptedException
	{
		departmentmngr.doOpenDepartmentManagerPage();
		departmentmngr.doOpenAddDepartmentPage();
		departmentmngr.doAddDepartment();
	}
	
	@Test(priority=2,enabled=false)
	public void doUpdateDepartmentTest() throws InterruptedException
	{
		departmentmngr.doUpdateDepartment();
	}
	
	@Test(priority=3)
	public void doAddDepartmentBlankField() throws InterruptedException
	{
		departmentmngr.doAddDepartmentBlankField();
	}
	
}
