package com.mycol.automation.tests;


import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.InviteFamilyMembersPage;
import com.mycol.automation.pagefactory.SignInPage;


public class InviteFamilyMembersTest extends TestBaseSetup {

	private WebDriver driver;
	private InviteFamilyMembersPage InviteFamilyMembers;
	private SignInPage signInPage;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		signInPage = new SignInPage(driver);
		InviteFamilyMembers = new InviteFamilyMembersPage(driver);
		
	}
	
	
	@Test(priority = 0, enabled = true)
	public void openInviteFamilyMembersPageFunction() throws InterruptedException {

		signInPage.openLoginPage();
		signInPage.clearLoginFields();
		signInPage.doLogin();
		InviteFamilyMembers.openInviteFamilyMembersPage();
		InviteFamilyMembers.verifyPageHeading();
	
	}

	@Test(priority = 1, enabled = true)
	public void verifyValidInviteFunction() throws InterruptedException {
		InviteFamilyMembers.openInviteFamilyMembersPage();
		InviteFamilyMembers.doInvite();
	//	Assert.assertTrue(InviteFamilyMembers.verifyInvite(),"Loged in user not verified");

	}
}
