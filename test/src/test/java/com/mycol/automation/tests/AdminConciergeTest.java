package com.mycol.automation.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.*;
import com.mycol.automation.pagefactory.AdminAddConcierge;
import com.mycol.automation.pagefactory.AdminEditConcierge;
import com.mycol.automation.pagefactory.AdminLogin;

import org.openqa.selenium.WebDriver;

public class AdminConciergeTest extends TestBaseSetup {

	private WebDriver driver;
	AdminAddConcierge admincon;
	AdminLogin signin;
	AdminEditConcierge editcon;

	@BeforeClass
	public void setup() {
		driver = getDriver();
		admincon = new AdminAddConcierge(driver);
		signin = new AdminLogin(driver);
		editcon = new AdminEditConcierge(driver);
		
	}

	@Test(priority = 0)
	public void openConciergesTest() {
		signin.doLogin();
		admincon.openConcierges();
	}

	@Test(priority = 1)
	public void doVerifyPageTitleTest() {
		Assert.assertTrue(admincon.doVerifyPageTitle());
	}

	@Test(priority = 2)
	public void doVerifyPageHeadingTest() {
		Assert.assertTrue(admincon.doVerifyPageHeading());
	}

	@Test(priority = 3)
	public void openAddConTest() {
		admincon.openAddCon();
	}

	@Test(priority = 4)
	public void verifyAddConPageHeadingTest() {
		admincon.verifyAddConPageHeading();
	}

	@Test(priority = 5)
	public void doAddConciergesTest() throws InterruptedException {

		admincon.doAddConcierge();

	}

	@Test(priority = 6,enabled= false)
	public void doFindUpdateUrlTest() {
		Assert.assertTrue(admincon.doFindUpdateUrl());
	}

	@Test(priority = 7)
	public void doVerifyAddConciergeTest() {
		Assert.assertTrue(admincon.doVerifyAddConcierge());
	}
	
	@Test(priority=8)
	public void doEditConciergeTest()
	{
		//editcon.doClearUpdate();
		editcon.doEditConcierge();
	}

	@Test(priority=9)
	public void doVerifyAddConcierge()
	{
	Assert.assertTrue(editcon.doVerifyAddConcierge());
	}
}
