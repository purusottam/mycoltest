package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.MyCases;
import com.mycol.automation.pagefactory.SignInPage;

public class MyCasesTest extends TestBaseSetup {
	private WebDriver driver;

	private SignInPage signInPage;
	private MyCases mycase;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		mycase = new MyCases(driver);
		signInPage = new SignInPage(driver);

	}

	@Test(priority = 0, enabled = true)
	public void doOpenMyCasePageTest() throws InterruptedException {
		signInPage.doLogin();
		mycase.doOpenMyCasePage();
	}

	@Test(priority = 1, enabled = true)
	public void doVerifyPageHeadingTest() {
		mycase.doVerifyPageHeading();
	}

	@Test(priority = 1, enabled = true)
	public void doAddFeedbackTest() {
		mycase.doAddFeedback();
	}
}
