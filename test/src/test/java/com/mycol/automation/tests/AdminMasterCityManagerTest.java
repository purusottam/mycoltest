package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.AdminLogin;
import com.mycol.automation.pagefactory.AdminMasterCityManager;

public class AdminMasterCityManagerTest extends TestBaseSetup {
	private WebDriver driver;
	
	AdminLogin signin;
	AdminMasterCityManager  citymngr;
	
	@BeforeClass
	public void setup() {
		driver = getDriver();
		signin = new AdminLogin(driver);
		citymngr = new AdminMasterCityManager(driver);
			}

	@Test(priority=0)
	public void doVerifyCityManagePageTest()
	{
		signin.doLogin();
		citymngr.doOpenManageCityManagerPage();
		citymngr.doOpenAddCityManagerPage();
		Assert.assertTrue(citymngr.doVerifyAddCityManagerPage());
	}
	
	@Test(priority=1)
	public void doVerifyAddCityPageTest() throws InterruptedException
	{
		citymngr.doOpenManageCityManagerPage();
		citymngr.doOpenAddCityManagerPage();
		citymngr.doAddCity();
	}
	
	@Test(priority=2,enabled=false)
	public void doUpdateCityTest() throws InterruptedException
	{
		citymngr.doUpdateCity();
	}
	
	@Test(priority=3)
	public void doBlankAllFieldTest() throws InterruptedException
	{
		citymngr.doAddCityBlankField();
	}
	
}
