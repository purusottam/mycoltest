package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.AdminLogin;
import com.mycol.automation.pagefactory.AdminMasterBrandManager;

public class AdminMasterBrandManagerTest extends TestBaseSetup {
	private WebDriver driver;
	
	AdminLogin signin;
	AdminMasterBrandManager  brandmngr;
	
	@BeforeClass
	public void setup() {
		driver = getDriver();
		signin = new AdminLogin(driver);
		brandmngr = new AdminMasterBrandManager(driver);
			}

	@Test(priority=0)
	public void doVerifyCityManagePageTest()
	{
		signin.doLogin();
		brandmngr.doOpenManageBrandsPage();
		brandmngr.doOpenAddBrandPage();
		Assert.assertTrue(brandmngr.doVerifyAddBrandPage());
	}
	
	@Test(priority=1)
	public void doVerifyAddCityPageTest() throws InterruptedException
	{
		brandmngr.doOpenManageBrandsPage();
		brandmngr.doOpenAddBrandPage();
		brandmngr.doAddBrand();
	}
	
	@Test(priority=2,enabled=false)
	public void doUpdateCityTest() throws InterruptedException
	{
		brandmngr.doUpdateBrand();
	}
	
	@Test(priority=3)
	public void doBlankAllFieldTest() throws InterruptedException
	{
		brandmngr.doAddBrandBlankField();
	}
	
}