package com.mycol.automation.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.auriga.automation.common.TestBaseSetup;
import com.mycol.automation.pagefactory.AdminFeedbackManagement;
import com.mycol.automation.pagefactory.AdminLogin;


public class AdminFeedbackTest extends TestBaseSetup {
	private WebDriver driver;

	AdminLogin adminsignin;
	private AdminFeedbackManagement feedback;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		feedback = new AdminFeedbackManagement(driver);
		adminsignin = new AdminLogin(driver);
	}

	@Test(priority = 0, enabled = true)
	public void doOpenFeedbackPopupTest() throws InterruptedException {
		adminsignin.doLogin();
		feedback.doOpenFeedbackPage();
		
	}

	@Test(priority = 1, enabled = true)
	public void doAddFeedbackTest() {
		feedback.doOpenFeedbackPopup();
		feedback.doAddFeedback();
	}

	@Test(priority = 2, enabled = true)
	public void doVerifyMandatoryFieldTest() {
		feedback.doOpenFeedbackPopup();
		feedback.doVerifyMandatoryField();
	}
}
